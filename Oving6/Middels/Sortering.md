### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Sortering

**Læringsmål:**

* Lister
* Sortering

**Starting Out with Python:**

* Kap. 7: Lists and Tuples

Sortering av tall er en vanlig aktivitet i dataverden. Det er derfor utviklet mange mer eller mindre avanserte algoritmer for å gjøre dette effektivt (få antall beregninger). 

<br>
<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Sortering.py](Sortering.py)*

I denne oppgaven skal vi implementere to enkle sorteringsalgoritmer: [Bubble Sort](https://en.wikipedia.org/wiki/Bubble_sort) og [Selection Sort](https://en.wikipedia.org/wiki/Selection_sort). 

## a)
Skriv funksjonen bubble_sort(list), forklart under.

* Så lenge listen er usortert:
 * Gå gjennom hele listen
   * Hvis elementet på plass i er større enn elementet på plass i+1
     * Bytt plass på de to elementene
     
Funksjonen skal returnere den sorterte listen.

**Eksempel på kjøring:**
```python
liste = [9,1,34,7,2,3,45,6,78,56,36,65,33,21,23,34,45,6]
print(bubble_sort(liste))
```

$-> [1, 2, 3, 6, 6, 7, 9, 21, 23, 33, 34, 34, 36, 45, 45, 56, 65, 78]$


<br>

<br>

## b)
Skriv funksjonen `selection_sort(list)`, forklart under.

* Lag en ny liste 
* Gå gjennom hele den usorterte listen
 * Finn det største tallet i den usorterte listen
   * Legg dette inn fremst i den nye listen
   * Fjern elementet fra den usorterte listen
   
Funksjonen skal returnere den sorterte listen.

**Eksempel på kjøring:**
```python
liste = [9,1,34,7,2,3,45,6,78,56,36,65,33,21,23,34,45,6]
print(selection_sort(liste))
```
$-> [1, 2, 3, 6, 6, 7, 9, 21, 23, 33, 34, 34, 36, 45, 45, 56, 65, 78]$



<br>

<br>

## c) Frivillig
Hvilken av de to algoritmene er "best"/raskest til å sortere? Her kan det være lurt å lese seg opp på "store O".
