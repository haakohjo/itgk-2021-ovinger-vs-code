# For å bruke hjelpefunksjonene er det to alternativer:
# 
# Kopiere funksjonene direkte til Kryptering.py filen
# Eller kopiere denne include linjen: from Kryptering_hjelpefunk import toHex, toString

import binascii
 
def toHex(word):
    return int(str(binascii.hexlify(word), 'ascii'), 16)
 
def toString(word):
    return str(binascii.unhexlify(hex(word)[2:]), 'ascii')