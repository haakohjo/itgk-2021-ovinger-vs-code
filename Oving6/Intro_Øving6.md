# Øving 6

**Læringsmål:**  

* Lister
* Tekstbehandling


**Starting Out with Python:**

* Kap. 7: Lists and Tuples
* Kap. 8: More About Strings

**Theory book:**

* Kap. 20: Internetworking: Concepts, Architecture, And Protocols 
* Kap. 21: IP - Internet Addressing

 * 21.1 - 21.13 (279 - 290)
 * 21.20 - 21.23 (295 - 299)
 

**Godkjenning:**

For å få godkjent øvingen må du gjøre ***4*** av ***14*** andre oppgaver. 

Oppgavene er markert med vanskelighetsgrad.

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!

<br>

Oppgave | Tema | Vanskelighetsgrad
--- | --- | ---
[Aksessering av karakterer i streng](Lett/Aksessering%20av%20karakterer.md) |Strenger| Lett
[Strenger og konkatinering](Lett/Strenger%20og%20konkatinering.md)|Strenger| Lett
[Slicing av strenger](Lett/Slicing%20av%20strenger.md)|Strenger|Lett
[Tekstbehandling](Lett/Tekstbehandling.md)|Strenger| Lett
[Strenghåndtering](Lett/Strenghåndtering.md)|Strenger, Løkker| Lett
[Innebygde funksjoner og lister](Lett/Innebygde%20funksjoner.md)|Lister| Lett
[Fjesboka](Lett/Fjesboka.md)|Lister, Strenger| Lett
[Ideel gasslov](Lett/Ideel%20gasslov.md)|Lister, Løkker, Strenger, Kjemi-oppgave | Lett
[Sammenhengende tallrekke](Lett/Sammenhengende%20tallrekke.md)|Lister, løkker| Lett
[Sortering](Middels/Sortering.md)|Lister, Algoritmer|Middels
[Strengmanipulasjon](Middels/Strengmanipulasjon.md)|Strenger, Lister| Middels
[Kryptering](Middels/Kryptering.md)|Lister, Sikkerhet| Middels
[Litt sjakk](Vanskelig/Litt%20sjakk.md)|2D-lister| Vanskelig