### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Fjesboka

**Læringsmål:**

* Lister
* Input
* Strenger

**Starting Out with Python:**

* Kap. 7: Lists and Tuples
* Kap. 8.3
* s. 379-380: Splitting a String
 

 <br>

 ## OPPGAVER
 Alle deloppgaver skal besvares her: *[Fjesboka.py](Fjesboka.py)* !

 I denne oppgaven skal du ta inn informasjon fra brukere på et gitt format, lagre dataen i lister og gjøre det mulig for brukeren å hente ut denne informasjonen igjen. 

<br>

## a)
Skriv en funksjon `add_data(user)` som skal ta inn en streng `user` på formatet: "given_name surname age gender relationship_status" og returnere dette i en liste.

Merk at alder skal lagres som en int, mens resten lagres som strenger. (Alder vil alltid ha indeks 2 i listen.)

<br>

## b)
Skriv en funksjon `get_person(given_name, facebook)` som tar inn et fornavn "given_name" og en liste "facebook" bestående av mange lister på formatet definert av `add_data(user)` og returnerer en liste bestående av alle personer med fornavnet **given_name.** 

```python
facebook = [["Mark", "Zuckerberg", 32, "Male", "Married"], 
            ["Therese", "Johaug", 28, "Female", "Complicated"],
            ["Mark", "Wahlberg", 45, "Male", "Married"],
            ["Siv", "Jensen", 47, "Female", "Single"]]
print(get_person("Mark", facebook))
```

skriver ut -> $[["Mark", "Zuckerberg", 32, "Male", "Married"], ["Mark", "Wahlberg", 45, "Male", "Married"]]$


<br>

**Hint** <br>
Fornavnet (indeks = 0) på den første personen (indeks = 0) i listen `facebook` kan finnes ved å skrive `facebook[0][0]`.
Det er mulig å iterere gjennom listen `facebook` i stedet for å bruke `facebook[m][n]`, der `m` og `n` er heltall.

<br>


## c)

Skriv funksjonen **main()** som lar brukeren legge til flere personer i en liste.

Dette gjøres ved å be brukeren om å legge til en **user** på formen "given_name surname age gender relationship_status" så lenge svaret ikke er "done".

Disse skal legges inn i en liste **facebook** ved hjelp av funksjonen `add_data(user)`.

Hvis svaret er "done" blir brukeren sendt videre til neste steg. 

Deretter skal brukeren kunne printe ut informasjon om brukerene fra listen basert på fornavn ved hjelp av funksjonen `get_person(given_name)`. 

Hvis svaret er "done" er programmet ferdig.

(Du kan velge om du vil printe ut listene eller skrive en melding til brukeren som vist i eksempelet under)

Test programmet ved å kjøre **main()**. 

**Eksempel på kjøring av kode:**

>*Hello, welcome to Facebook. Add a new user by writing "given_name surname age gender relationship_status". <br>
Add a new user:* Therese Johaug 29 Female Complicated <br>
*Add a new user:* Mark Zuckerberg 33 Male Married <br>
*Add a new user:* done <br>
*Ok<br>
Search for a user:* Therese <br>
*Therese Johaug is 29 years old, her relationship status is Complicated. <br>
Search for a user:* Alf <br>
*[] <br>
Search for a user:* Mark <br>
*Mark Zuckerberg is 33 years old, his relationship status is Married. <br>
Search for a user:* done <br>


