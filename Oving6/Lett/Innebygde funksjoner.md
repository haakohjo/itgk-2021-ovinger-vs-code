### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Innebygde funksjoner og lister

**Læringsmål:**

* Lister
* Innebygde funksjoner

**Starting Out with Python:**

* Kap. 7: Lists and Tuples
 
 <br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Innebygde funksjoner.py](Innebygde%20funksjoner.py)* !

I denne oppgaven skal vi bruke innebygde funksjoner i Python til å behandle lister. 

Du finner informasjon og nyttige metoder [her](https://docs.python.org/3/tutorial/datastructures.html). (bruk disse!)

<br>

## a)
Opprett en liste random_numbers bestående av 100 tilfeldige heltall fra 1 til og med 10.

**Eksempel på slik liste:**
```python
[9, 7, 4, 3, 2, 3, 9, 4, 4, 1, 1, 3, 4, 9, 5, 5, 3, 4, 7, 8, 9, 4, 6, 6, 1, 6, 5, 7, 2, 5, 7, 1, 9, 4, 9, 6, 1, 1, 1, 5, 6, 5, 9, 10, 5, 7, 4, 4, 10, 4, 4, 8, 1, 5, 4, 9, 5, 5, 2, 8, 10, 8, 1, 5, 10, 8, 3, 3, 7, 7, 6, 3, 3, 3, 4, 9, 4, 8, 4, 6, 1, 10, 3, 6, 5, 7, 10, 9, 9, 1, 10, 3, 2, 3, 6, 9, 8, 3, 2, 5]
```


<br>

## b)
Skriv kode for å finne antall forekomster av tallet **2** i listen.
Eksempl listen i oppgave a, ville her gitt output 5. 


<br>

## c)
Skriv ut summen av alle tallene i listen. For listen i a ville denne koden gitt output 529

<br>

## d)
Sorter listen i stigende rekkefølge.

**Eksempel på kjøring for listen i a):**
```python
[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10]
```

<br>

## e)
Skriv ut typetallet. (Det tallet det er flest forekomster av i listen). For listen i a ville svaret vært 4.

<br>

## f) 
Skriv ut listen baklengs.
