### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Strenghåndtering

**Læringsmål:**

* Strenger
* Løkker

**Starting Out with Python:**

* Kap. 8: More About Strings
 

I denne oppgaven skal vi lære å sammenligne, reversere og sjekke om en streng er et palindrom.
<br>
<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Strenghåndtering.py](Strenghåndtering.py)* ! Det følger med en testkode til samtlige deloppgaver.

## a)
Lag en funksjon `check_equal(str1, str2)` som sjekker om to strenger er like ved å sammenligne dem tegn for tegn. Funksjonen returnerer `True` hvis strengene er like; `False` ellers. 

<br>

## b)
Lag en funksjon `reversed_word(string)` som tar inn en streng, reverserer den og returnerer den reverserte strengen. Dette skal gjøres tegn for tegn med en løkke.

<br>

## c)
Et palindrom er et ord som staves likt begge veier (eks. “abba”). Lag en funksjon `check_palindrome(string)` som returnerer `True` om en streng er et palindrom; `False` ellers.

**Hint** <br>
Bruk metoder du har laget tidligere som hjelpemetoder.

<br>

## d)
Lag en funksjon `contains_string(str1, str2)` som tar inn to strenger og sjekker om den første strengen inneholder den andre. Dersom den gjør det, returner **posisjonen** den andre strengen begynner på (fra 0). Returner **-1** ellers.





