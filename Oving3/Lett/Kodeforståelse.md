### **[Intro øving 3](../Intro_Øving3.md)**
<br>

# Kodeforståelse

**Læringsmål:**

* Tolke kode

**OPPGAVER**

I disse oppgavene skal du ikke selv skrive noe kode, men derimot analysere og tenke deg fram til hva kodesnuttene skriver ut når de kjøres. Oppgavene besvares ved å skrive hva kodesnuttene gjør som en kommentar i python-fila oppgitt under. 

Alle deloppgaver besvares her: *[Kodeforståelse.py](Kodeforståelse.py)* !
<br><br>
Ca. 20% av eksamen er kodeforståelse, så se på dette som god trening! 

## a)
Hva skrives ut i koden under?
```python
a=345
b=''
while a or b=='':
    b=str(a%2)+b
    a=a//2
print(b)
```

<br>

## b)
 Hva skrives ut i koden under?

```python
for x in range(0, 10, 2):
    print(x, end='')
    if x%4==0:
        print( ": Dette tallet går opp i 4-gangern")
    else:
        print()
```
     
**Tips:** <br>
`end=''` gjør at det neste som printes ikke printes en linje under, men at det fortsetter på samme linje.

<br>

## c)
Hva skrives ut i koden under?

```python
i = 1
while i<10:
    i = i*2
print(i)
```

<br>

## d)
Hva skrives ut i koden under?
```python
i = 1
j = 3
while j>0:
    i = i*2
    j = j - 1
print(i)
```

<br>


## e) 
Hva skrives ut i koden under?

```python
i = 5
for x in range(i):
    for y in range(x+1):
        print("*", end="")
    print()
```
    
Her er det en dobbel løkke, så dette er nok nytt for mange. Prøv likevel! Hvordan fungerer egentlig en løkke i en løkke? Se side 176 i Starting Out with Python.

