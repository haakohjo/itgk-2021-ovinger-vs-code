### **[Intro øving 3](../Intro_Øving3.md)**
<br>

# Tekstbasert spill 2

**Læringsmål:**

* Løkker
* Betingelser

<br>

## OPPGAVER

Alle deloppgaver besvares her: *[Tekstbasert spill 2.py](Tekstbasert%20spill%202.py)* !
<br><br>

I denne oppgaven skal vi utvide spillet vi begynte på i øving 2. Du må ikke ha gjort oppgaven i øving for å gjøre denne, men det kan være lurt å ta en kikk [her](../../Oving2/Middels/Tekstbasert%20spill.md) for å forstå hva oppgaven går ut på. 

## a)

Ved hjelp av en while løkke, la brukeren skrive inn kommando, og gjenta tilstanden dersom brukeren gir inn en ugyldig kommando. Se forrige oppgave for oppførsel ellers. 

**Eksempel:**

>*Du står utenfor en dør.* sadasd <br>
*Forstår ikke kommando, prøv noe annet. <br>
Du står utenfor en dør.* Gå inn <br>
*Du går inn døren.* <br>

<br>






## b)

Ved hjelp av løkker ønsker vi nå at visse kommandoer skal ta brukeren tilbake til utgangspunktet i spillet.  Dersom en bruker skriver inn en kommando som ikke skal ta brukeren ut av spillet (eller en ugyldig kommando), ønsker vi at den opprinnelige meldingen brukeren får skal gjentas, ellers går spilleren ut av løkken. **Altså, for alle andre kommandoer enn en spesifikk skal løkka gjentas.** I eksempelet under gjentas tilstanden helt til brukeren skriver noe som endrer den.

**Eksempel:** <br>

>*Du står utenfor en dør med en postkasse.* Bank på <br>
*Du får ingen respons. <br>
Du står utenfor en dør med en postkasse.* Gå andre veien <br>
*Du snur deg og vandrer hjem igjen. Du hører en skummel lyd og løper tilbake.* <br>
*Du står utenfor en dør med en postkasse.* Åpne døren <br>
*Du går inn døren.* <br>

<br>


## c)

Ved hjelp av løkker og variabler skal du nå la visse kommandoer brukeren skriver inn endre tilstanden i spillet (altså variablene) **selv om vi ikke går ut av løkka**. Ved å sjekke tilstanden til disse variablene skal en kommando kunne gjøre to ting, utifra hva tilstanden til en variabel er satt til.

**Eksempel:** <br>

>*Du står utenfor en dør med en postkasse.* Åpne døren <br>
*Døren er låst. <br>
Du står utenfor en dør med en postkasse.* Åpne postkassen <br>
*Du finner en nøkkel. <br>
Du står utenfor en dør med en postkasse.* Åpne døren <br>
*Du låser opp døren og går inn.* <br>

<br>


## d)

Legg til muligheten for brukeren til å gå ut av spillet uansett tilstand vet å kun trykke enter uten å skrive inn noe. Legg inn en avslutningsmelding.

**Eksempel:** <br>

>*Du står utenfor en dør med en postkasse. <br>
Ha en fin dag!*

<br>

**Hint:** <br>
Du kan bruke break for å hoppe ut av en løkke.



