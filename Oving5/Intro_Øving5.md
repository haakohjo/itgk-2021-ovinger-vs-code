# Øving 5

**Læringsmål:**  

* Lister
* Nettverk

**Starting Out with Python:**

* Kap. 7: Lists and Tuples

**Theory Book:**

* Kap. 2: Internet Trends (197 - 205)
* Kap. 8: Reliability And Channel Coding
* Kap. 13: Local Area Networks: Packets, Frames, And Topologies

* Kap. 20: Internetworking: Concepts, Architecture, And Protocols 
 

**Godkjenning:**

For å få godkjent øvingen må du gjøre 4 av 11 andre oppgaver. 

Oppgavene er markert med vanskelighetsgrad.

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!

Oppgave | Tema | Vanskelighetsgrad
--- | --- | ---
[Generelt om lister](Lett/Generelt%20om%20lister.md) |Lister|Lett  
[Lett og blandet om lister](Lett/Lett%20og%20blandet%20om%20lister.md)|Lister|Lett 
[Kodeforståelse](Lett/Kodeforståelse.md)|Kodeforståelse|Lett 
[Vektorer](Lett/Vektorer.md)|Lister|Lett 
[Lister og løkker](Lett/Lister%20og%20løkker.md)|Lister og løkker|Lett 
[Teoridelen på eksamen](Lett/Teoridelen%20på%20eksamen.md)|Lister|Lett 
[Gangetabell og lister](Lett/Gangetabell%20og%20lister.md)|2D-Lister|Lett
[Lotto](Middels/Lotto.md)|Lister|Middels
[Tannfeen](Middels/Tannfeen.md)|2D-lister, Løkker|Middels
[Chatbot](Middels/Chatbot.md)|Lister, Input| Middels
[Matriseaddisjon](Middels/Matriseaddisjon.md)|2D-Lister|Middels