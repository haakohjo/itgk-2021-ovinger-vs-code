### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Matriseaddisjon

**Læringsmål:**

* Lister av flere dimensjoner

**Starting Out with Python:**

* Kap. 7.8


## OPPGAVER 
Denne oppgaven skal løses her: *[Matriseaddisjon.py](Matriseaddisjon.py)* !

I denne oppgaven skal du implementere noen funksjoner slik at to matriser blir skrevet ut på et fint format og addert med hverandre. 

En matrise er et godt eksempel på en todimensjonal liste av tall som har et bestemt antall rader x og et bestemt antall kolonner y. Du kan tenke på en matrise som en liste med x antall elementer, hvor hvert element er en liste av størrelse y. I denne oppgaven skal vi se på matriseaddisjon. I denne oppgaven definerer vi addisjon av to matriser på samme måte som med vektoraddisjon: dersom vi skal summere matrise a med b og kaller den nye matrisen for c, vil *c[x][y]* være *a[x][y]* + *b[x][y]*, hvor *c[x][y]* er elementet i matrisen c som befinner seg på rad *x* og kolonne *y*.

Du får gitt en main-funksjon med følgende kode (ligger også ved i .py-filen):

```python
def main():
    A = random_matrise(4,3)
    print_matrise(A, 'A')
    B = random_matrise(3,4)
    print_matrise(B, 'B')
    C = random_matrise(3,4)
    print_matrise(C, 'C')
    D = matrise_addisjon(A,B)
    E = matrise_addisjon(B,C)
    print_matrise(E, 'B+C' )
```

som, etter at du har gjort oppgaven, skal gi følgende utskrift:

>A=[ <br>
   [1, 8, 4, 3] <br>
   [5, 1, 5, 8] <br>
   [9, 5, 8, 0] 
  ] <br>
B=[ <br>
   [7, 3, 3] <br>
   [2, 1, 7] <br>
   [2, 2, 3] <br>
   [3, 5, 9]
  ] <br>
C=[ <br>
   [4, 4, 6] <br>
   [1, 9, 0] <br>
   [9, 8, 5] <br>
   [2, 9, 5] <br>
  ] <br>
Matrisene er ikke av samme dimensjon # A og B har ulike dimensjoner (ulikt antall rader og kolonner) <br>
B+C=[ <br>
     [11, 7, 9] <br>
     [3, 10, 7] <br>
     [11, 10, 8] <br>
     [5, 14, 14]
    ]

    
    
**Din oppgave**

Implementer funksjonene `random_matrise(bredde, høyde)`, `print_matrise(matrise, navn)` og `matrise_addisjon(a, b)` slik at utskriften av `main()` gir utskriften over.

1. Funksjonen `random_matrise(bredde, høyde)` skal returnere en matrise med *høyde* antall rader og *bredde* antall kolonner. Matrisen skal være fylt med tilfeldige tall fra og med 0 til 10. Bruk random-biblioteket.
2. `print_matrise(matrise, navn)` skal printe ut matrisene på samme format som vist i utskriften over. 
3. `matrise_addisjon(a, b)` skal ta inn to matriser og returnere en matrise som er summen av a og b. Dersom to matriser er av ulik størrelse er det umulig å addere matrisene og "Matrisene er ikke av samme dimensjon" skal skrives ut. 

