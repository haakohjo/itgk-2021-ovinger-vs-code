### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Osteviruset

**Læringsmål:**
- Dictionary

**Starting Out with Python:**
- Kap. 9.1 Dictionaries

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Osteviruset.py](Osteviruset.py)* !


Ostelageret til Tine benytter et smart system som mapper oster med lagerhylleplass. Ostene er indeksert på navn, og hver type ost er mappet til en tuple med hylleplasser hvor nøyaktig én ost av denne typen befinner seg. Vedlagt kodesnutt viser en liten bit av Tine sin ostedatabase i form av en dictionary. 

Indeksering av dictionaries er ganske likt indeksering av lister. Den største forskjellen er at indekser (nøkler) kan i tillegg til tall, også være andre datatyper. I denne oppgaven er nøklene strenger. 

Merk: I denne oppgaven trenger du ikke lage noen ekstra funksjoner. Bruk gjerne innebygde funksjoner/metoder som dict.items og str.split for å løse oppgaven.

<br>

## a)
Finn og skriv ut alle hylleplasser til osten “port salut”. Dvs. skriv ut verdien til denne nøkkelen.

**Riktig utskrift skal være:**

('B15-1', 'B15-2', 'B15-3', 'B15-4', 'B16-1', 'B16-2', 'B16-4')

<br>

## b)
Dessverre så har hyllene fra B13 til B15, samt hyllene A234, A235 og C31 blitt infisert av et ostespisende virus! Finn alle typer oster som potensielt er smittet av viruset og legg disse til i en liste `infected_cheeses`.

**Utskrift av infected_cheeses bør nå se slik ut:** <br>
['mozarella', 'camembert', 'cheddar', 'port salut']

<br>

## c)
Tross osteviruset, ønsker Tine fortsatt å selge ost til det sultne norske folk. Finn alle typer ost der ingen individ er smittet av viruset, og skriv ut resultatet på formen <hylleplass\> <ostetype\>.

**Riktig utskrift ser slik ut:**

```
ZLAFS55-4  gombost  
ZLAFS55-9  gombost  
GOMBOS-7   gombost  
A236-4     gombost  
SPAZ-1     geitost  
SPAZ-3     geitost  
EMACS45-0  geitost  
GOMBOS-4   ridder   
B16-3      ridder
```