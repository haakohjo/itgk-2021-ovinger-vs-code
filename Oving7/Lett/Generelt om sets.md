### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Generelt om sets

**Læringsmål:**
- Sets
- Lister

**Starting Out with Python:**
- Kap. 9.2

<br>

## INTRODUKSJON

### **Sets**


Et set inneholder en samling av unike verdier og fungerer på samme måte som et set i matematikken. Forskjellen på et set og en liste er at en liste kan inneholde flere like elementer, for eksempel:
```python
liste = [1,2,3,1,2,3]
```
Altså kan vi finne alle tallene to ganger. Dette går ikke i et set ettersom alle elementene i et set er ulike. Tilsvarende set ser slik ut:
```python
my_set = set([1,2,3])
```

På samme måte som vi kunne opprette en dictionary ved å skrive `my_dict = dict()`, kan vi opprette et set ved å skrive:
```python
my_set=set()
```
For å legge til et element i et set kan vi benytte oss av `add()`. For å legge til flere elementer på samme gang, kan `update()` benyttes. For å fjerne et element kan vi benytte oss av `remove()` eller `discard()`. Forskjellen mellom `remove()`og `discard()` er at `discard()` ikke kaster en error hvis elementet ikke finnes i settet. 

```python
my_set.add(1)
my_set.update(2,3,4)

my_set.remove(1)
my_set.discard(2)
```
De fleste ting fungerer likt mellom sets og lister, og her er noen eksempler:
- iterering gjennom setet/listen
- sjekke om et element er i setet/listen
- finne lengden til setet/listen
  
<br>

Dersom du har hatt sannsynlighet er du kanskje godt kjent med venndiagram. Da har du sikkert hørt om union og snitt. Dersom vi har to sets:
```python
set1 = set([1,2,3,4,5,6,7,8])
set2 = set([0,2,4,6,8,10,12])
```
kan vi se at begge setene inneholder 2, 4, 6 og 8. Dette er det som kalles intersection, eller snitt. På figuren under kan vi se at snittet er det feltet hvor sirklene overlapper.

![img](Resources/Sets.png)

For å finne snittet av set1 og set2 kan vi skrive som under.

```python
set3 = set1.intersection(set2)
set3 = set2.intersection(set1)
set3 = set1&set2
```
Alle de tre linjene i kodeblokken over er ekvivalente.

Union er et annet nyttig ord, og det vil si tallene som enten er i set1 eller set2 eller begge, dvs. alle tallene som er med. For å finne unionen av set1 og set2 kan vi skrive:

```python
set3 = set1.union(set2)
set3 = set2.union(set1)
set3 = set1 | set2
```
Her gjør også alle de tre kodelinjene akkurat det samme.

Tallene som er i set1, men ikke i set2, dvs. 1, 3, 5 og 7 kan vi finne ved å skrive:
```python
set3 = set1.difference(set2)
set3 = set1-set2 # denne linja og linja over gjør akkurat det samme
```
For å finne elementene som er i set2 men ikke set1 er det bare å bytte om på set1 og set2 i koden over.
Symmetric difference vil si alle tallene som er i set1 eller set2 (dvs. unionen) minus snittet (tallene som er i begge setene). I dette tilfelle er det 0,1,3,5,7,10 og 12.

Dette finner vi slik:
```python
set3 = set1.symmetric_difference(set2)
set3 = set2.symmetric_difference(set1)
set3 = set1^set2               #set3 = (0,1,3,5,7,10,12)
```
Her gjør også de tre linjene akkurat det samme.

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Generelt om sets.py](Generelt%20om%20sets.py)* !

## a)
Lag et tomt set som heter `my_set`, legg til alle oddetallene opp til 20 i setet ved å bruke en for-løkke og print setet

Har du gjort det riktig skal output være: <br>
{1, 3, 5, 7, 9, 11, 13, 15, 17, 19}

<br>

## b)
Lag et nytt set som heter `my_set2` og inneholder alle oddetallene frem til 10.

<br>

## c)
Lag et setet `my_set3` som inneholder alle tallene som er i setet fra a) men ikke i setet fra b) 

<br>

## d)
Dersom du tar snittet av setet fra b) og setet fra c), hva forventer du å få da? Hva med a) og c)?

<br>

## e)
Bruk de innebygde funksjonene `len()` og `set()` til å lage en funksjon `allUnique(lst)`, som returnerer `True` om listen `lst` inneholder kun unike elementer og ellers returnerer `False`.

Her er det lagt ved en testkode i .py filen.

<br>

## f)
Bruk de innebygde funksjonene `list()` og `set()` til å lage en funksjon `removeDuplicates(lst)`, som fjerner duplikater fra listen `lst` og returner den modifiserte listen.

Her er det lagt ved en testkode i .py filen.