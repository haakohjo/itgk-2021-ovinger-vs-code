### **[Intro øving 7](../Intro_Øving7.md)**

<br>


# Innebygde funksjoner i dictionaries


**Læringsmål:**
- Dictionaries
- Innebygde funksjoner

**Starting Out with Python:**
- Kap. 9.1


<br>

## INTRODUKSJON

### **Innebygde funksjoner**
Akkurat som at det eksisterer mange innebygde funksjoner for lister, eksisterer det også mange innebygde funksjoner for dictionaries. Fra øving 5 husker du kanskje at du ble introdusert for len() som returnerer lengden til en liste, dvs. antall elementer i listen. Denne funksjonen kan også benyttes på dictionaries:

```python
dictionary = {}
dictionary['red'] = 'primærfarge'
num_items = len(dictionary) 
dictionary['blue'] = 'primærfarge'
dictionary['green'] = 'sekunærfarge'
num_items2 = len(dictionary)
```
Her vil num_items bli 1 og num_items2 bli 3. 

Dersom du prøver å få tak i en verdi fra en nøkkel som ikke er lagt inn, vil du få en feilmelding. For å hindre dette kan det være lurt å sjekke om nøkkelen er i dictionaryen. Dette kan gjøres ved bruk av en enkel if-setning:

```python
if 'yellow' in dictionary:
    #do something
```

Man kan også fjerne elementer ganske greit i en dictionary, ved bruk av del og pop() som under:

```python
if 'green' in dictionary:
    del dictionary['green']  # green: sekundærfarge blir nå slettet fra dictionaryen
```

Dersom det er ønskelig å tømme hele dictionaryen kan en bruke `dictionary.clear()`.

Ettersom en dictionary består av elementer som inneholder både en nøkkel og en verdi, er det litt mer avansert å iterere gjennom en dictionary enn en liste.

La oss tenke oss at vi har følgende dictionary:

```python
my_family={'bror': 'Martin', 'søster': 'Helene', 'mor': 'Anne', 'far': 'Bob Bernt', 'hund': 'Lovise'}
```

Dersom du ønsker å iterere gjennom alle nøklene i dictionaryen, dvs. 'bror', 'søster', 'mor', 'far' og 'hund' kan du skrive for-løkken som under.

```python
for key in my_family:
    print(key)
```
Dersom du ønsker å skrive ut alle nøklene samt navnene til familiemedlemmene kan du skrive:

```python
for key in my_family:
    print(key, my_family[key])
```
En annen måte å få denne utskriften på, altså en iterering som itererer gjennom både verdier og nøkler samtidig, er ved å bruke følgende format:

```python
for key, value in my_family.items():
    print(key, value)
```
`items()` returnerer alle nøklene i en dictionary samt deres tilhørende verdier.

Dersom det hadde vært ønskelig å kun printe ut verdiene, dvs. navnene, kunne man benyttet seg av `values()`:

```python
for val in my_family.values():
    print(val)
```

<br>

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Innebygde funksjoner i dictionaries.py](Innebygde%20funksjoner%20i%20dictionaries.py)* !

## a)
Gitt følgende dictionary:

```python
scores = {'Amanda': [88, 92, 100], 'Kennet': [30, 45, 50], 'Einstein': [100,100,100]}
```

1. hva skrives ut dersom vi skriver `print(scores['Amanda'])`
2. hva skrives ut dersom vi skriver `print(scores['Amanda'][2])`


<br>

## b)
Opprett en tom dictionary som kalles fruit, for så å legge til tre frukter du liker som nøkler, og antall frukt av denne typen du pleier å spise hver dag som verdi. Print ut dictionaryen.

<br>

## c)
Legg til to frukter som du misliker og fjern de tre fruktene du liker. Benytt deg av de innebygde funksjonene `del()` og `dict[key]=value`.

<br>

## d)
Skriv ut begge verdiene (antall frukt du spiser hver dag som du misliker) i dictionaryen.

<br>

## e)
Sjekk om 'bananer' er i dictionaryen, og fjern 'bananer' fra dictionaryen dersom den er oppført. Her har du fått utdelt en påbegynt kode, fyll inn din kode på markert sted.

<br>

## f)
Legg til et par bær som du liker i dictionaryen og skriv ut både verdiene og nøklene i dictionaryen på et greit format.

