### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Tallak teller antall tall

**Læringsmål:**
- Dictionary
- Lese fra fil

**Starting Out with Python:**
- Kap. 6: Files and Exceptions
- Kap. 9: Dictionaries and Sets


## OPPGAVER
Alle deloppgaver skal besvares her: *[Tallak.py](Tallak.py)* !


I denne oppgaven skal du hjelpe tallentusiasten Tallak med å lese inn data fra filen numbers.txt. Denne filen inneholder en liste med tall, noe som er Tallaks store lidenskap. Filen kan du se her: *[numbers.txt](Resources/numbers.txt)* !

## a)
Tallak ønsker først og fremst å få en oversikt over størrelsen på filen:

Lag funksjonen `number_of_lines(filename)` som returnerer totalt antall linjer i filen med navn `filename`. Test funksjonen med `numbers.txt`. Du bør få 36 som svar. 

**Hint**: <br>
`Oving7/Lett/Resources/numbers.txt` er filens "Relative path".

<br>

## b)
Tallak ønsker deretter en liste som inneholder antall forekomster av hvert enkelt tall i listen:
    
Lag funksjonen `number_frequency(filename)` som returnerer en dictionary der hver nøkkel er et tall, og verdien til en nøkkel er antall forekomster av det tallet i filen med navn `filename`. Test funksjonen med *numbers.txt*

*Riktig utskrift:* <br>
{7: 4, 4: 7, 9: 4, 1: 3, 3: 5, 5: 7, 2: 4, 6: 1, 8: 1}

<br>

## c)
Tallak er derimot ikke helt fornøyd med utskriften enda:

Kall funksjonen `number_frequency` på *numbers.txt*. For hvert tall i resultatet skal du skrive ut en linje med tallet sammen med antall forekomster, separart med et kolon.

**Eksempel på output fra kjøring:**
```
7: 4
4: 7
9: 4
1: 3
3: 5
5: 7
2: 4
6: 1
8: 1
```

PS: output trenger ikke å være sortert på noen måte.
