### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Generelt om dictionaries

**Læringsmål:**
- Dictionary

**Starting Out with Python:**
- Kap. 9: Dictionaries


## INTRODUKSJON

### **Dictionaries**

En dictionary er et objekt som lagrer en samling av data, og kan minne mye om en liste. Det som skiller en dictionary fra en liste er at hvert element i en dictionary består av to deler: En nøkkel(a key) og en verdi(a value). En nøkkel peker til en verdi, og man bruker nøkkelen for å hente ut verdien.

- key: Nøklene kan være flyttall, heltall strenger eller tupler. Nøklene må være unike.
- values: Innholdet som nøkkelen peker til. Dette kan være lister, strenger, heltall, osv.

For eksempel kan du tenke på en engelsk-norsk ordbok hvor engelske ord er ramset opp i alfabetisk rekkefølge, og til hvert engelske ord finner du en rekke norske alternativer for dette ordet.

En dictionary opprettes på to måter

- dictionary = dict()
- dictionary = {}, til forskjell fra lister er det her krøllparenteser(bracets) og ikke hakeparenteser.

Et eksempel på bruk av dictionaries er gitt nedenfor:

```python
telephone_numbers = {'arne': 99748391, 'knut': 74839220, 'siri': [92835674, 65748329]}  # Oppretter en dictionary med tre oppføringer
print(telephone_numbers['knut'])  # Skriver ut knut sitt telefonnummer
print(telephone_numbers['siri'][1])  # Skriver ut det andre telefonnummeret i listen av Siri sine telefonnummer
```

Her er 'arne' den første nøkkelen, og 99748391 er den tilhørende verdien. 'knut' er den andre nøkkelen og 'siri' er den siste nøkkelen.

For å legge til et nytt key-value-par i dictionarien kan du gjøre følgende:

```python
telephone_numbers['stian'] = 82935829  # Legger Stian til i dictionarien
print (telephone_numbers)  # Skriver ut hele telefonlisten
```

Dersom du skal ha tak i en verdi, må du slå opp på nøkkelen. La oss si at du ønsker å vite nummeret til arne, da må du skrive følgende:

```python
telephone_numbers['arne']
```

<br>

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Generelt om dictionaries.py](Generelt%20om%20dictionaries.py)* !
## a)
Opprett en tom dictionary `my_family`.

<br>

## b)
Lag en funksjon `add_family_member(role, name)` som tar inn to strenger, `rolle`(key) og `navn`(value), og legger familiemedlemmet til i dictionaryen `my_family`.

For å teste koden din så kan du kjøre vedlagt kode, hvis du har gjort alt riktig skal output bli:

$->{'bror': 'Arne'}$ <br>
$->{'bror': 'Arne', 'far': 'Bob Bernt'}$


<br>

<br>

## c)
Hvis du ønsker å legge til flere familiemedlemmer med samme rolle (f.eks. to brødre), må du lage en liste som inneholder alle dine brødre.

Når du har gjort dette kan du bruke vanlige liste-metoder som .append() og .pop() for å legge til eller fjerne elementer fra listen.

*Nå skal du utvide funksjonaliteten fra b) slik at du kan legge til flere familiemedlemmer med samme rolle.*


**Hint:** 
- Du må ha en liste med navn for hver rolle.

- For å sjekke om en rolle finnes fra før av kan du bruke "if key in dict". Du kan også løse dette med unntakshåndtering.

For å teste koden din så kan du kjøre vedlagt kode, hvis du har gjort alt riktig skal output bli:


$->{'mor': ['Anne'], 'bror': ['Arne', 'Geir']}$


<br>

<br>

## d) Frivillig
*(Du trenger ikke gjøre denne for å få godkjent oppgaven)* <br>
Skriv ut den ferdige dictionarien på et fint format.

**Hint:** <br>
Du kan bruke en for-løkke med syntaksen for key, value in my_family.items(): for å iterere gjennom dictionarien. 