### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Generelt om filbehandling

**Læringsmål:**
- Filbehandling
- Betingelser
- Løkker

**Starting Out with Python:**
- Kap. 6: Files and Exceptions

I denne oppgaven skal vi skrive til en fil og lese fra en fil.

## INTRODUKSJON

### **Filer**
Det er ofte nyttig å kunne lagre data til en fil, eller lese data fra en fil når man skriver et program i Python. De mest brukte funksjonene er for åpning, redigering og lukking av eksterne filer. 

Når du åpner filen må du spesifisere hvordan du skal bruke filen. Det er derfor viktig å åpne filen på riktig måte. Måten dette gjøres på er som inn-parameter i `open()`-funksjonen, noen eksempler er:

- **'r'** - for lesing av filen (default)
- **'w'** - for skriving til filen
- **'a'** - for å legge til data (**a**ppend) til den eksisterende filen

I denne oppgaven skal vi bli bedre kjent med hvordan dette fungerer:

- For å åpne en fil i Python kan vi skrive: `f = open('filename', Bruksmåte)`. Bruksmåte er enten `'r'`, `'w'` eller `'a'` avhengig av hva hvordan filen skal brukes.
- For å lese data fra en fil kan vi bruke: `innhold = f.read()`
- For å legge til data til en fil kan vi skrive: `f.write(data)`

Filer lukkes på følgende måte: `f.close()`

<br>

### **Lesing av fil**
Eksempelet under viser lesing av en fil.
```py
# LESING AV FIL
f = open('example_file1.txt','r') #r spesifiserer at man skal lese fra en fil
innhold = f.read()
print(innhold)
f.close()
```

## **Skriving til fil**
For å skrive til en fil kan man gjøre slik som under.
```py
f = open('example_file1.txt','w')  #w spesifiserer at filen skal skrives til
f.write('En hatefull ytring')
f.close()
```

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Generelt om filbehandling.py](Generelt%20om%20filbehandling.py)* !


## a)
Lag en funksjon `write_to_file(data)` som tar inn strengen `data` og legger denne inn i en fil **my_file.txt**. Hvis du får problemer så kan du prøve å bruke: `Oving7/Lett/Resources/my_file.txt` isteden for kun `my_file.txt`.


<br>

## b)
Lag en funksjon `read_from_file(filename)` som tar inn strengen `filename` med filnavnet og skriver ut innholdet.

Du kan teste ut funksjonen ved å kalle den med `'my_file.txt'` som argument.


<br>

## c)
Lag en funksjon `main()` hvor bruker får valget mellom å skrive til fil eller lese fra fil. Funksjonen skal kjøre så lenge brukeren ikke svarer `'done'`. (Se eksempelkjøring)

- Hvis brukeren velger **write** skal du bruke **a)** til å skrive data til **my_file.txt**
- Hvis brukeren velger **read** skal du skrive ut innholdet (dersom det er noe) i **my_file.txt** vha. **b)**

**Eksempel på kjøring:**

>*Do you want to read or write?* write <br>
*What do you want to write to file?* hei allan <br>
*hei allan was written to file. <br>
Do you want to read or write?* read <br>
*hei allan <br>
Do you want to read or write?* done <br>
*You are done.* <br>


Du kan når som helst sjekke innholdet i fila her: *[my_file.txt](Resources/my_file.txt)* !

