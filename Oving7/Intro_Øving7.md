# Øving 7

**Læringsmål:**

- Dictionary
- Sets
- Lese fra fil
- Skrive til fil
- Lister

**Starting Out with Python:**
- Kap. 6: Files and Exceptions
- Kap. 9: Dictionaries and Sets

**Teori:**

*Merk: Kun deler av kapitlene er pensum*

- 25: TCP - Reliable Transport Service
- 27: Network Performance (QoS And DiffServ)
- 29: Network Security (361 - 384)
- 32: The Internet Of Things

**Godkjenning:**

For å få godkjent øvingen må du gjøre **4 av 10** andre oppgaver.

Oppgaver som er ekstra vanskelig er markert med (stjerne).
    
Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!

Oppgave|Tema|Vanskelighetsgrad
---|---|---
[Generelt om dictionary](Lett/Generelt%20om%20dictionaries.md)|Dictionaries|Lett
[Innebygde funksjoner](Lett/Innebygde%20funksjoner%20i%20dictionaries.md)|Dictionaries|Lett
[Generelt om sets](Lett/Generelt%20om%20sets.md)|Sets|Lett
[Generelt om filbehandling](Lett/Generelt%20om%20filbehandling.md)|Filhåndtering|Lett
[Osteviruset](Lett/Osteviruset.md)|Dictionaries|Lett
[Bursdagsdatabasen](Lett/Bursdagsdatabasen.md)|Dictionaries, Exceptions|Lett
[Tallak teller antall tall](Lett/Tallak.md)|Filhåndtering, Dictionaries | Lett
[Opptaksgrenser](Middels/Opptaksgrenser.md)|Filhåndtering, Dictionaries|Middels
[Søke i tekst](Middels/Søke%20i%20tekst.md)|Filhåndtering, Dictionaries|Middels
[Tre på rad](Middels/Tre%20på%20rad.md)|Repetisjon: Spill|Middels

