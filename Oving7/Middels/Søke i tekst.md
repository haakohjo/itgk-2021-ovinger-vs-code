### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Søke i tekst

**Læringsmål:**

* Lese fra fil
* Tekstbehandling
* Dictionary

**Starting Out with Python:**

* Kap. 6: Files and Exceptions
* Kap. 9: Dictionaries and Sets
 
<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Søke i tekst.py](Søke%20i%20tekst.py)* !

I denne oppgaven skal du bli bedre kjent med dictionaries og lesing fra fil. Bruk gjerne innebygde funksjoner, alt er lov!


## a)
Lag en funksjon **`read_from_file(filename)`** som tar inn en string (**filename**) og returnerer innholdet i filen. Dersom funksjonen tar inn 'alice_in_wonderland.txt' bør utskriften bli følgende:


```
     Alice's Adventures in Wonderland

        ALICE'S ADVENTURES IN WONDERLAND

                Lewis Carroll

       THE MILLENNIUM FULCRUM EDITION 3.0




                CHAPTER I

            Down the Rabbit-Hole

.
.
.

```

<br>

## b)
Lag en funksjon **`remove_symbols(text)`** som fjerner alle spesialtegn fra teksten **text**, og gjør alle bokstaver små (lowercase).   
Ting som skal fjernes er slik som tall, komma, punktum og fnutter. La mellomrom stå slik at du kan skille ord fra hverandre.

Dersom funksjonen tar inn 'alice_in_wonderland.txt' bør utskriften bli følgende:

**Kjøring av kode:** <br>
```
alices adventures in wonderland
alices adventures in wonderland                          
lewis carroll ...
```

<br>

## c)
Lag en funksjon **`count_words(filename)`** som tar inn en streng (**filename**), teller antall forekomster av alle ord i filen og returnerer en dictionary med resultatet.

**Hint**:
Bruk en dictionary med **ord** som nøkkel og **antall forekomster** som value.


<br>

## d)
Sjekk om oppgave c) fungerer ved å kjøre funksjonen med 'alice_in_wonderland.txt' som argument.

*Du kan skrive ut hver linje i dictionarien for seg ved å bruke:*

```python
alice_dict = countWords('alice_in_wonderland.txt')
for word, value in alice_dict.items():
    print(word, value)
```