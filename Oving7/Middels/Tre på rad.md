### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Tre på rad

**Læringsmål:**

* Lister
* Funksjoner 
* Betingelser
* Strenger

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Tre på rad.py](Tre%20på%20rad.py)* !
 
I denne oppgaven skal du implementere det populære spillet 3 på rad. Spillet er for to spillere; x og o, som plasserer brikker . En spiller vinner om den klarer å få 3 på rad, enten horisontalt, vertikalt eller diagonalt.

## a)
Lag en funksjon som skriver ut brettet, det kan f.eks. se slikt ut om du vil:

```python
    1   2   3
  -------------
1 |   |   |   |
  -------------
2 |   |   |   |
  -------------
3 |   |   |   |
  -------------
```

<br>

## b)
Lag en funksjon som sjekker om en spiller har vunnet.

<br>

## c)

Lag en funksjon som tar inn navnene til de to brukerne.

<br>

## d)
Lag en funksjon som sjekker om et trekk er lovlig, altså at det ikke finnes andre brikker der.

<br>

## e)
Lag en funksjon som sjekker at input fra brukeren er riktig, altså at man ikke skriver inn rare tegn, eller skriver inn koordinater utenfor spillebrettet.

<br>

## f)
Sett dette sammen til et fullverdig 3 på rad spill!


