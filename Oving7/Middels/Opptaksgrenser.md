### **[Intro øving 7](../Intro_Øving7.md)**

<br>

# Opptaksgrenser

**Læringsmål:**

* Lese fra filer
* dictionaries

**Starting Out with Python:**

* Kap. 6: Files and Exceptions
* Kap. 9.1 Dictionaries


<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Opptaksgrenser.py](Opptaksgrenser.py)* !

I denne oppgaven skal vi lese inn en fil med opptaksgrensene fra Samordna Opptak.

Filen er på CSV-format (Comma Separated Values), noe som betyr at hver linje er en liste med felter separert med komma. Tekstfelter er omsluttet av fnutter (").

* Første felt er studiets navn
* Andre felt er poenggrensen (enten et tall, eller "Alle" dersom alle kom inn)

F.eks. linjen: **"NTNU 194459 Antikkens kultur","Alle"** sier at alle som søkte kom inn på Dragvoll-studiet “Antikkens kultur” ved NTNU.

Hver funksjon i de følgende deloppgavene tar data fra filen **poenggrenser_2011.csv** som input. Derfor er det veldig praktisk å lagre innholdet i en variabel, slik at du slipper å lese den på nytt hver gang.

## a)
Les fra fila `poenggrenser_2011.csv` og lagre innholdet i en variabel.


<br>

## b)
Skriv en funksjon som finner ut hvor mange studier som tok inn alle søkere. 

***Husk at du nå i alle deloppgavene kan bruke variabelen du definerte i a!***

*Eksempel på kjøring av kode:*
```python
Antall studier hvor alle kom inn: 590
```

<br>

## c)
Skriv en funksjon som finner gjennomsnittlig opptaksgrense for NTNU. Ikke ta med studier som tok inn alle søkere.

*Eksempel på kjøring av kode:*
```python
Gjennomsnittlig opptaksgrense for NTNU var: 46.29
```


<br>

## d)
Skriv en funksjon som finner studiet med laveste opptaksgrense (som IKKE tok inn alle søkere).

*Eksempel på kjøring av kode:*
```python
Studiet som hadde den laveste opptaksgrensen var: AHO 189343 Industridesign
```

<br>

## e)
Lag en dictionary som har studiestedet som nøkkel og en liste med dictionaries som verdi. Denne listen med dictionaries skal ha navnet på linjen som nøkkel og opptakspoengene til den tilsvarende linjen som verdi. Dersom en linje har navnet "Fysikk og Matematikk" trenger du kun å ta hensyn til det første ordet, dvs. "Fysikk". 

**Eksempel på utskrift:**

```python
ATH [{'Kristendom': ' Alle'}, {'Interkulturell': ' Alle'}, {'Musikk': ' Alle'}, {'Teologi': ' Alle'}, {'Kristendom': ' Alle'}, {'Psykologi': ' Alle'}, {'Musikk': ' Alle'}, {'Interkulturell': ' Alle'}, {'Psykologi': ' Alle'}, {'Praktisk': ' Alle'}]
AHO [{'Arkitekt': '12.3'}, {'Industridesign': '11.7'}]
BDH [{'Sykepleierutdanning': '45.5'}]
MF [{'Kristendom/RLE': ' Alle'}, {'Samfunnsfag': ' Alle'}, {'Interkulturell': ' Alle'}, {'Teologi': ' Alle'}, {'Religion': ' Alle'}, {'Ungdom': ' Alle'}, {'Lektor-': ' Alle'}, {'Teologi': ' Alle'}]
DHS [{'Sykepleierutdanning': '48.3'}, {'Vernepleierutdanning': '41.8'}, {'Sosialt': '49.1'}, {'Sosialt': '42.4'}, {'Ergoterapeututdanning': '32.6'}]
DMMH [{'Førskolelærerutdanning': '36.3'}, {'Førskolelærer': '39.1'}, {'Førskolelærer': '44'}, {'Førskolelærer': '46.2'}, {'Førskolelærer': ' Alle'}]
.
.
.
UIT [{'Ingeniør': ' Alle'}, {'Ingeniør': ' Alle'}, {'Ingeniør': ' Alle'}, {'Ingeniør': ' Alle'}, {'Sykepleierutdanning': '43.8'}, {'Lærerutdanning': ' Alle'}, {'Lærerutdanning': ' Alle'}, {'Førskolelærerutdanning': ' Alle'}, ....
```
