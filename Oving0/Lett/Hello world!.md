# Hello world!

**Læringsmål:**

* Her listes opp læringsmålene for den aktuelle øvingen.

**Starting Out with Python:**

* Her listes opp kapilter/delkapitler i boka som er relevante for øvingen. 

<BR>

## INTRODUKSJON

Under introduksjon får man en innføring i relevant pensum til den spesifikke øvingsoppgaven. Dette er i større grad utbredt i de lette øvingsoppgavene. 

Eksempel for denne oppgaven kunne vært:

### **Print ()**
Programmer som skal brukes av mennesker, må ofte vise informasjon på skjermen. En enkel måte for dette i Python er funksjonen print().
Nedenstående kode gir en mest mulig selvforklarende intro til print-setningen.

```python
print('Det som skal ut på skjermen, settes inni parentesen bak print.')
print('Tekst må omsluttes med fnutter (apostrof)')
print("eller med dobbelfnutter (hermetegn).")
print("Tall trenger ikke fnutter rundt seg:")
print(42)
print('En blank linje kan printes med tom parentes:')
print()
print('Du kan printe flere ting', 'med komma mellom:', 5, 6)
print('Desimaltall må skrives med punktum i Python:', 3.14)
print('Hvis du bruker komma, tolkes det som to adskilte tall:', 3,14)
print('Komma inni en tekst kommer ut på skjermen: ,,,,,', 'komma', 'mellom', 'tekster gjør ikke det.')
```

<BR>

## OPPGAVER
Alle deloppgaver skal løses i [Hello world.py](Hello%20world.py)!

## a)

Lag et program som skriver ut "Hello world!" til skjermen.
