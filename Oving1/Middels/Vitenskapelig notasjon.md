### **[Intro øving 1](../Intro_Øving1.md)**
<br>

# Vitenskapelig notasjon

**Læringsmål:**

* Vitenskapelig notasjon for spesielt små og spesielt store flyttall

**Starting Out with Python:**

* Kap. 2.8

## INTRODUKSJON

### **Formatering av store tall**
Når vi skriver flyttall i Python, er det enkleste ofte å skrive tallet rett frem, for eksempel 9.80665 eller 0.0002. Hvis tallet blir veldig stort (~$10^{6}$) eller lite (~$10^{-6}$), er det derimot tidkrevende å skrive tallet fullt ut, samtidig som at det lett dukker opp slurvefeil. Da vil resultatet av en beregning bli helt feil. Derfor er det vanlig å bruke vitenskapelig notasjon for store og små tall.

I Python kan vi skrive tallene 3.0 × 10<sup>9</sup> og 3.19 × 10 <sup>-10</sup> ved bruk av multiplikasjon(`*`) og potensregning(`**`) slik: `3.0*10**9` og `3.19*10 **(-10)`. Det vil gi riktig resultat, men kaster bort tid og strøm på å gjøre helt unyttige beregninger, først en potensering, så en multiplikasjon, for å finne fram til et tall vi egentlig allerede vet hva er. Det er derfor bedre å bruke notasjonen `3.0e9` og `3.19e-10`, hvor tallet bak `e` viser tierpotensen for å uttrykke store og små tall. 

Notasjonen med `e` (som her ikke har noe med konstanten e å gjøre) lar oss legge tallet rett inn i variabelen uten noe regning.

## OPPGAVER
Alle deloppgaver skal besvares her: *[Vitenskapelig notasjon.py](Vitenskapelig%20notasjon.py)* !

## a) Fysikk / kjemi

Avogadros konstant 6.022 × 10<sup>23</sup> sier hvor mange molekyler av et stoff som fins i ett mol av stoffet. Lag et program som ber brukeren navngi et stoff hun er i besittelse av, oppgi hvilken molvekt dette stoffet har, og hvor mye av stoffet hun har, og så skriver ut på skjermen hvor mange molekyler brukeren har av stoffet. Bruk gjerne `format()`-funksjonen for å unngå for mange desimaler i svaret. 

Eksempel på kjøring:

  
```python
Si et stoff du er i besittelse av: vann
Hva er molvekt i gram for vann? 18
Hvor mange gram vann har du? 500
Du har 1.7e+25 molekyler vann
```

#### **Hint**
`print()` tillater at en varibel som f.eks. `x` kan listes opp sammen med tekststrenger fordi `print()` kan ta flere argumenter. Funksjonen `input()` kan derimot bare ta ett argument, så hvis du ønsker navnet på stoffet med i ledeteksten for de neste input-setningene, må det plusses med annen tekst heller enn å listes opp med komma.


<br>

## b) Musikk
Antall mulige melodilinjer på 10 toner (inkludert rytmevariasjoner) er anslått til å være 8.25 x 10<sup>19</sup>, jfr https://plus.maths.org/content/how-many-melodies-are-there. Lag et program i samme stil som 60m-programmet over som spør brukeren hvor mange ulike ti-toners melodilinjer hun selv tror hun har komponert og / eller hørt, og skriver ut som resultat hvor stor andel dette utgjør av antall mulige melodier (her vil dette sluttresultatet sannsynligvis bli et svært lite tall). 

Eksempel på kjøring:

  
```python
Antall ulike 10-toners melodilinjer du har hørt? 3288
Du har hørt 3.985454545454546e-15 prosent av melodier som er mulig.
```