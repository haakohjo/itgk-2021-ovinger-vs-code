### **[Intro øving 1](../Intro_Øving1.md)**
<br>

# Peppes Pizza

**Læringsmål:**

* Gjøre enkle kalkulasjoner i python
* Skrive et enkelt program

**Starting Out with Python:**

* Kap. 2

## OPPGAVER
Alle deloppgaver skal besvares her: *[Peppes pizza.py](Peppes%20pizza.py)* !

**Introduksjon**

Du har nettopp spist middag på Peppes Pizza med noen venner, og mottar denne kvitteringen:

Pizza: 750kr<br>
Studentrabatt: 20%<br>
Tips: 8%


## a) 
Lag et program som lagrer verdiene fra regningen i variabler. Variablene skal altså være `Pizza`, `Studentrabatt` og `Tips`.

## b)
Lag en variabel `totalt` som er lik den totale summen av middagen. Du kan skrive i sammen med oppgave a.

## c)
Gjør slik at brukeren kan skrive inn hvor mange som deltok på middagen, og print ut hvor mye hver person må betale.

**Eksempel på kjøring:**

  
```python
Total pris: 648.0
Hvor mange deltok på middagen? 6
Ettersom dere var 6 personer, så må hver person betale 108.0 kroner.
```
<br><br>

#### **Hint**:
For at man skal bruke input i beregninger må den gjøres om til en int.
