### **[Intro øving 1](../Intro_Øving1.md)**
<br>

# Jeg elsker ITGK!

**Læringsmål:**

* Printe tekst og tall til konsoll

* Skrive et enkelt program

**Starting Out with Python:**

* Kap. 1.5

* Kap. 2.2-2.4

#### *Alle deloppgaver skal besvares her: [Jeg elsker ITGK.py](Jeg%20elsker%20ITGK.py)!*  
  
<Br>

## a)

**Lag et program som gir følgende utskrift til skjermen:**

```
Jeg elsker ITGK!
```
<Br>

## b)

Lag et program som bruker fire print-setninger for å skrive informasjonen nedenfor til skjermen (linje 2 skal være blank). Tallene skal skrives ut som tall, **ikke** som en del av en tekststreng omgitt av fnutter.

```python
Norge
 
Areal (kv.km): 385180
 
Folketall (mill.): 5.3
```
<Br>

## c)

**Lag et program som skriver ut på skjermen teksten:** 
```
"Jeg elsker ITGK" ropte studenten da 1c funket.
```

Hermetegnene rundt teksten skal være med i det som kommer ut på skjermen. 

<Br>

## d)

**Lag et program som skriver ut på skjermen teksten vist under.**

Hint: For å slippe å skrive så mye, kopier teksten inn i boksen under, så trenger du bare selv å skrive print, parenteser og passende fnutter rundt teksten.

**Tekst til oppgave d:**
```
Noen barn sto og hang ved lekeplassen.
Diskusjonstemaet deres var noe uventet.

- Hvorfor heter'e "Python"?
- Var'e slanger som laget det? 
- Nei, Guido van Rossum.
- Likte slanger kanskje da? - Nei, digga "Monty Python".
- Hva er det? Et fjell?
- Nei, engelsk komigruppe. Begynte i '69
- Wow! Var'e fremdeles dinosaurer da?
```

<Br>

## e)

Legg til to kommentarer i koden din fra b) og c) hvor du forklarer hva som ble gjort i hver av oppgavene.  Legg også til kommentaren `print('Hei")` i en av deloppgavene.

Merk at dersom du hadde skrevet print('Hei") i koden ville ikke koden kjørt. Dette skyldes at det først blir brukt en enkel fnutt og deretter en dobbel fnutt. Så lenge dette er skrevet i en kommentar slik det ble gjort i e) vil det ikke påvirke koden og alt går fint. 

<Br>

## f)
Koden i kodeblokken under kjører ikke pga. syntaksfeil i alle print-funksjonene. Din oppgave er å rette opp i feilene slik at koden kjører. Oppgaven skal besvares i form av kode i samme fil som de øvrige oppgavene. 

```python
print("Heihei, jeg vil visst ikke kompilere jeg :(')

print("Halla, så "bra" du ser ut i dag")

print(Hei på deg)

print "Er ikke dette gøy?")

```
