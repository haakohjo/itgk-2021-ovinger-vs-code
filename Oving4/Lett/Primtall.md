### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Primtall

**Læringsmål:**
- Løkker

**Starting Out with Python:**
- Kap. 4.2-4.3

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Primtall.py](Primtall.py)* ! Det er lagt ved en testkode for oppgave a og b i .py filen.


## a)
Skriv funksjonen `divisable(a, b)` som returnerer `True` dersom `a` er delelig med `b` og `False` ellers.

<br>

## b)
Lag funksjonen `isPrime` som tar inn et tall `a`. Funksjonen skal ha en for-løkke som itererer fra *b = 2,3,...,a-1* og bruke funksjonen fra forrige oppgave for å sjekke om `a` er delelig med `b`. Om de er delelige avslutter du og returnerer `False`. Ellers skal du returnere `True`.

<br>

## c)
Algoritmen skal nå gjøres raskere ved hjelp av 2 små inngrep som beskrevet nedenfor. Kall den nye funksjonen for isPrime2 og ta utgangspunkt i koden du skrev i b).

- Du skal nå bare iterere deg gjennom oddetall. Men du må fortsatt sjekke om a er delelig med 2.
- Istedenfor å gå helt til **i = a−1**,<br> skal du nå avslutte når **i > round(sqrt(a)+0.5)**
