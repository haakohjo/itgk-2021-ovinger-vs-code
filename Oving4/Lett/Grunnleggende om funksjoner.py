print("Grunnleggende om funksjoner")


print("Oppgave a)")
#TODO endre koden under slik beskrevet i oppgaveteksten.

print("Data fra spørreundersøkelse")
print()                                         ##
print("**  **  **  **  **  **  **  **  **")     ##
print("  **  **  **  **  **  **  **  **")       ##
print()                                         ##
print("Del 1: ... div. data her, ikke vist")
print()                                         ##
print("**  **  **  **  **  **  **  **  **")     ##
print("  **  **  **  **  **  **  **  **")       ##
print()                                         ##
print("Del 2: ... mer data ...")
print()                                         ##
print("**  **  **  **  **  **  **  **  **")     ##
print("  **  **  **  **  **  **  **  **")       ##
print()                                         ##
print("Del 3: ... enda mer data ...")
print()                                         ##
print("**  **  **  **  **  **  **  **  **")     ##
print("  **  **  **  **  **  **  **  **")       ##
print()                                         ##
print("Del 4: ... ytterligere data ...")



print("Oppgave b og c)")
#TODO Fyll inn/endre slik beskrevet i oppgaveteksten.

def komparativ(adj):
    # GROVT FORENKLET FOR Å KUNNE FOKUSERE PÅ HOVEDPOENGET
    if len(adj) >= 8: # unøyaktig
        svar = "mer " + adj
    else:
        svar = adj + "ere"
    return svar
  
### TILLEGG 1, ny funksjon, kommer her:
 
#DEL AV KODEN HVOR SYSTEMET DISSER BRUKEREN
adj = input("Beskriv deg selv med et adjektiv: ")
print("Hah! Jeg er mye", komparativ(adj), "!")
  
### TILLEGG 2 kommer her ...
 
# DEL AV KODEN HVOR BRUKEREN TRENER PÅ Å GRADBØYE
adj = input("Skriv inn et adjektiv: ")
svar = input("Hva er komparativ for" + adj + "? ")
fasit = komparativ(adj)
if svar == fasit:
    print("Korrekt!")
else:
    print("Feil, det var", fasit)
  
### TILLEGG 3 kommer her...


