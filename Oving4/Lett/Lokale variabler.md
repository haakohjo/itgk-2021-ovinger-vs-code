### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Lokale variabler

**Læringsmål:**
- Kodeforståelse
- Funksjoner

**Starting Out with Python:**
- Kap. 5.4

## INTRODUKSJON

### **Lokale variabler**
En lokal variabel er en variabel som blir lagd inne i en funksjons kodeblokk, og som ikke kan benyttes av kall utenfor funksjonen. Koden under viser et eksempel på dette. Hva tror du er galt?

```python
def getName():
    name = input("Hva heter du? ")  

getName()
print("Hei,",name)
```

Her er `name` en lokal variabel for funksjonen `getName()`. Dette medfører at koden i linje 5 ikke kan aksessere variabelen `name` slik den prøver, og at programmet krasjer. Vi får her feilmeldingen "NameError: name 'name' is not defined", som rett og slett betyr at programmet ikke har kjennskap til noen variabel eller funksjon ved navnet 'name'.

For å få koden til å fungere slik vi ønsker kan vi returnere name-variabelen og tilordne denne til en egen variabel - som kan kalles name eller noe helt annet - som linje 5 har tilgang til. Koden kan da se ut som under.

```python
def getName():
    name = input("Hva heter du? ")
    return name
  
name = getName()
print("Hei,",name)
````
Koder kan også inneholde parametere som også fungerer som lokale variabler. Eksempel:

```python
def printName(name):
    print("Hei ", name)
  
 
printName("Ola")
````
Her vil "Ola" bli til name-variabelen og kun være tilgjengelig innenfor funksjonen.

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Lokale variabler.py](Lokale%20variabler.py)* !

## a)
Alle kodesnuttene under inneholder en feil, men en av dem vil fortsatt kjøre uten å krasje; hvilken og hvorfor? **Prøv å finne feilen i hver kodesnutt.** Skriv svaret ditt som en kommentar i .py filen.
```python
#Kodesnutt 1:
def cupcakes():
    cupcakes = 24
    print("Jeg har laget",cupcakes,"cupcakes")

def cakes():
    print("Men jeg har bare bakt",cake,"kake")
    cake = 1

cupcakes()
cakes()

#Kodesnutt 2:
def cupcakes():
    cupcake = 1
    print("Jeg har laget",cupcake,"cupcake")

def cakes():
    print("Og jeg har bakt",cupcake,"kake")

cupcakes()
cakes()

#Kodesnutt 3:
def cupcakes():
    cupcakes = 24
    print("Jeg har laget",cupcakes,"cupcakes")

def cakes():
    print("Men jeg har bare bakt",cake,"kake")

cupcakes()
````
<br>

## b)

I denne oppgaven skal du skrive to funksjoner. Den første skal ta inn to argumenter `x` og `y`, lage en variabel `num` = `x//y`, og returnere resultatet slik at programmet kan skrive til skjerm "Heltallsdivisjon av `x` over `y` gir `num`".

Den andre funksjonen skal ta inn ett argument x, lage en varibel num = x**2, og returnere resultatet slik at programmet kan skrive til skjerm "Kvadratet av `x` er `num`".

**Eksempel på kjøring:**

>*Heltallsdivisjon av 23 over 4 gir 5 <br>
Kvadratet av 3 er 9*



<br>

## c)
Begge funksjonene i b) har en variabel kalt num. Kan dette føre til noen problemer i koden? Hvorfor/Hvorfor ikke? Svar som kommentar i .py filen.