### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Sekantmetoden

**Læringsmål:**
- Funksjoner
- Løkker

**Starting Out with Python:**
- Kap. 4.2-4.3
- Kap. 5.5
- Kap. 5.8-5.9

I denne oppgaven skal vi implementere Sekantmetoden i Python.

Sekantmetoden kan benyttes for å finne nullpunkt til en matematisk funksjon. En matematisk funksjon har et nullpunkt der $f(x) = 0$, dvs. at grafen krysser x-aksen. 

Sekantmetoden er gitt ved:

$x_{k+1}=x_k-f(x_k)\frac{x_k-x_{k-1}}{f(x_k)-f(x_{k-1})}$


<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Sekantmetoden.py](Sekantmetoden.py)* !

<br>

## a)
Lag en funksjon `f` som tar inn et tall `x` som argument og returnerer verdien til

$f(x)=(x-12)e^{x/2}-8(x+2)^{2}$

og en annen funksjon `g` som tar inn `x` som argument og returnerer verdien til

$g(x)=-x-2x^{2}-5x^{3}+6x^{4}$

Test også koden din med et par verdier. Du kan for eksempel sjekke at `f(0)` returnerer `-44` og at `g(1)` returnerer `-2`.

<br>

## b)

Sekantmetoden er en tilnærming av Newtons metode:

$x_{k+1}=x_{k}-\frac{f(x_{k})}{f'(x_{k})}$ hvor

$f'(x_k) \approx \frac{f(x_{k})- f(x_{k-1})}{x_{k}-x_{k-1}}$

Lag en funksjon `differentiate(x_k, x_k1, func)` som bruker formelen for den approksimerte (**f'(x)**) gitt i oppgaveteksten til å derivere. Funksjonen skal ta inn tre argumenter: `x_k`, `x_k1` og `func`, og returnere den deriverte (et flyttall):

- `x_k`: punktet hvor vi ønsker å finne den deriverte
- `x_k1`: et tidligere punkt som vi bruker for å finne stigningstall
- `func`: funksjonen man ønsker å derivere (i denne oppgaven vil `func` alltid tilsvare enten `f` eller `g` fra **a)**)

For å teste funksjonen kan du kjør funksjonskallet `differentiate(9,10,f)`. dette skal returnere: -210.7749243035878

<br>

## c)
Lag en funksjon `secant_method(x0, x1, func, tol)` som benytter seg av `differentiate(x_k, x_k1, func)` til å utføre sekantmetoden. Funksjonen skal returnere verdien(/avslutte) når endringen i `x` er mindre enn toleransegrensen `tol`. 

Test funksjonen din med følgende verdier:
```python
secant_method(11,13,f,0.00001)
secant_method(1,2,g,0.00001)
secant_method(0,1,g,0.00001)
```

**Eksempel på kjøring av kode:**

>Funksjonen nærmer seg et nullpunkt når x = 13.92 , da er f(x) =  -1.59e-06 <br>
Funksjonen nærmer seg et nullpunkt når x =  1.22 , da er f(x) =  -9.66e-08 <br>
Funksjonen nærmer seg et nullpunkt når x =  0.0  , da er f(x) =  0.0 <br>
