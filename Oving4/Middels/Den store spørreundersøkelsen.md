### **[Intro øving 4](../Intro_Øving4.md)**

<br>

## Den store spørreundersøkelsen

**Læringsmål:**

* Funksjoner 
* Betingelser
* Løkker

**Starting Out with Python:**

* Kap. 3.1-3.2
 * 3.1: The if Statement
 * 3.2: The if-else Statement 
* Kap. 4.2
 * 4.2: The while Loop: A Condition-Controlled Loop
* Kap. 5.4-5.6, 5.8 
 * 5.4: Local Variables
 * 5.5: Passing Arguments to Functions
 * 5.6: Global Variables and Global Constants
 * 5.8: Writing Your Own Value-Returning Functions
 
<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Den store spørreundersøkelsen.py](Den%20store%20spørreundersøkelsen.py)* !

<br>

I denne oppgaven skal du implementere et lite utdrag av en spørreundersøkelse om leksevaner til studenter. Undersøkelsen er beregnet for kvinner og menn i aldersgruppen 16-25 år.

Spørreundersøkelsen implementeres ved hjelp av å skrive ut spørsmål og lese input fra brukeren i en while-løkke som kjøres så lenge brukeren ønsker å skrive inn data. Når alle spørsmålene er besvart, gjentas spørsmålene for en ny person. 

Hold styr på fem globale tellere (variabler) under kjøringen av while-løkken: **antall_kvinner**, **antall_menn**, **antall_fag**, **antall_ITGK**, og **antall_timer_lekser**. Hvis brukeren til enhver tid svarer hade på et spørsmål skal while-løkken avsluttes og verdiene av de fem tellerne skrives ut.

I programmet ditt kan du få bruk for:

```python
from sys import exit        #Du kan nå bruke funksjonen exit() i programmet ditt. Anbefales brukt i sjekk_svar-funksjonen.
```

exit() vil avslutte kjøringen av programmet.

Bruk funksjoner til å lese inn svar fra brukeren (de bør være forskjellige om brukeren skriver inn et tall eller en streng), og til å skrive ut statistikk til slutt (oppgave g).

Funksjoner som kan implementeres i løpet av oppgaven er:

sjekk_svar(spm) <br>
les_streng(spm) <br>
les_ja_nei(spm) <br>
les_tall(spm) <br>
skriv_statistikk() <br>
(Der spm er svaret på de forskjellige spørsmålene som skal stilles i while-løkken)

Deloppgave a til e skal altså implementeres inni while-løkken.

## a)
Be brukeren skrive inn kjønn og alder. Med for eksempel ***f*** for kvinne og ***m*** for mann, lagre disse i variablene **kjonn** og **alder**.

<br>

## b)
Sjekk om brukeren er innenfor tiltenkt aldersgruppe. Dersom brukeren er utenfor aldersgruppen, skriv en melding om at vedkommende ikke kan ta spørreundersøkelsen, og hopp tilbake til første spørsmål (kjønn).

<br>

## c)
Dersom brukeren er innenfor tiltenkt aldersgruppe, spør om brukeren tar et **fag**, med svaralternativene ja og nei. Deretter lagres svaret som en variabel fag. Ta utgangspunkt i denne variabelen når du avgjør om brukeren trenger å svare på de neste to spørsmålene eller ikke. Om noe annet svares, skal spørsmålet gjentas til et gyldig svaralternativ er gitt. 


<br>

## d)
Du skal nå spørre om brukeren tar ITGK og lagre svaret som en variabel **medlem_ITGK**, men spørsmålsteksten skal variere ut ifra alderen på brukeren

* Dersom brukeren er under 22 år skal spørsmålsteksten være: Tar du ITGK?
* Ellers skal spørsmålsteksten være: Tar virkelig du ITGK?


<br>

## e)
Spør brukeren hvor mange timer han/hun bruker i snitt daglig på lekser og lagre svaret i variabelen **timer_lekser**


<br>

## f)
Endre programmet ditt slik at de globale tellerne nevnt tidligere i oppgaven blir inkrementert i deloppgavene a til e.


<br>


## g)
Kjør programmet ditt, utfør noen undersøkelser, og avslutt ved å skrive hade. Sjekk at statistikken (verdiene av de globale tellerne) som skrives ut på slutten er korrekt.


<br>

## h)
Vil det være mulig å hente ut svarene fra spørreundersøkelsen etter at vi har avsluttet programmet? Hvor er de i så fall lagret? Eventuelt hvorfor ikke?

<br>

<br>

<br>



**Eksempel på kjøring:**
```
Velkommen til spørreundersøkelsen!
 
 Hvilket kjønn er du? [f/m]: f
 Hvor gammel er du?: 21
 Tar du et eller flere fag? [ja/nei ]: bleh
 Tar du et eller flere fag? [ja/nei ]: ja
 Tar du ITGK? [ja/nei]: ja
 Hvor mange timer bruker du daglig (i snitt) på lekser?: 2
 
Velkommen til spørreundersøkelsen!
 
 Hvilket kjønn er du? [f/m]: m
 Hvor gammel er du?: 28
Du kan dessverre ikke ta denne undersøkelsen
 
Velkommen til spørreundersøkelsen!
 
 Hvilket kjønn er du? [f/m]: m
 Hvor gammel er du?: 25
 Tar du et eller flere fag? [ja/nei ]: ja
 Tar du virkelig ITGK?: nei
 Hvor mange timer bruker du daglig (i snitt) på lekser?: 3
 
Velkommen til spørreundersøkelsen!
 
 Hvilket kjønn er du? [f/m]: f
 Hvor gammel er du?: 18
 Tar du et eller flere fag? [ja/nei ]: nei
 
Velkommen til spørreundersøkelsen!
 
 Hvilket kjønn er du? [f/m]: m
 Hvor gammel er du?: 24
 Tar du et eller flere fag? [ja/nei ]: hade
 
Resultat av undersøkelse!
 Antall kvinner: 2
 Antall menn: 2
 Antall personer som tar fag: 2
 Antall personer som tar ITGK: 1
 Antall timer i snitt brukt på lekser : 2.5
 ```