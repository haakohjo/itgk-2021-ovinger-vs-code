### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Not quite Blackjack


**Læringsmål:**
- Funksjoner
- Betingelser
- Løkker

**Starting Out with Python:**
- Kap. 3: Decision Structures and Boolean Logic 
- Kap. 4.2: The while Loop
- Kap. 5: Functions
  
 <br>

## OPPGAVE
Oppgaven skal besvares her: *[Not quite Blackjack.py](Not%20quite%20Blackjack.py)* !

I denne oppgaven skal du lage spillet Blackjack med en vri. Vanlige Blackjack reglene som skal implementeres er som følger:

- Et ess teller enten 1 eller 11
- Alle bildekort (J, Q, K) har verdi 10
- Du får alltid utdelt to kort til å begynne med
- Hvis den samlede verdien på kortene er over 21 er du ute
- Et ess med 10 eller et bildekort er en «ekte» blackjack
- Du vinner på én av tre måter:
  - Du får ekte blackjack uten at dealer gjør det samme,
  - Du oppnår en høyere hånd enn dealer uten å overstige 21, eller
  - Din hånd har verdi mindre enn 21, mens dealerens hånd overstiger 21

Det som er annerledes med vår Blackjack, er at dealer bare får to kort, og at spilleren ikke får velge verdien ess vil ha. Spillet vil automatisk ta den verdien som er nærmest 21, men som ikke overstiger 21, og så fort en verdi for ess er satt, vil ikke denne endres senere. Dvs. at om man først får 1 (ess) og 8, vil verdien bli satt til 11+8=19. Om man deretter velger å trekke enda et kort og får 4, vil verdien bli 19+4=23, og man vil tape. Det blir altså ikke tatt hensyn til at 1+8+4<21. Om man derimot først fikk 4 og 8, og deretter fikk 1 (ess), så ville verdien blitt 4+8+1=13. 

**Eksempel på kjøring:**

>*Dealers cards are 9 and ? <br>
Your score is: 16 <br>
Do you want another card? (J/N)* J <br>
*Your score is: 19 <br>
Do you want another card? (J/N)* N <br>
*Dealers score is: 18 <br>
You won!* <br>
  
>*Dealers cards are 10 and ? <br>
Your score is: 20 <br>
Do you want another card? (J/N)* N <br>
*Dealers score is: 21 <br>
You lost*
  
>*Dealers cards are 10 and ? <br>
Your score is: 15 <br>
Do you want another card? (J/N)* J <br>
*You got: 25 <br>
You lost*
