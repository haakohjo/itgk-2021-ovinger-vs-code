# Øving 8

**Læringsmål:**

- Rekursjon
- Sammensatte programmer

**Starting Out with Python:**

- Kap. 12: Recursion

**Theory book**

- Kap 5 - Algorithms
 

## Godkjenning (LES DETTE NØYE):

**TDT4109:** Oppgaven "Rekursjon" er, i tillegg til teorien på Blackboard, obligatorisk for å få godkjent øvingen. I tillegg må man gjøre minst én av disse tre: "Eksamen Python 2012", "Sudoku" eller "Sjakk". Hvis man gjør mer enn én, vil dette tilsvare to øvinger godkjent. Sjakk-oppgaven teller som to øvinger.

**TDT4110:** Oppgaven "Matplotlib" er, i tillegg til teorien på Blackboard, obligatorisk for å få godkjent øvingen. I tillegg må man gjøre minst én av disse fire: "Eksamen Python 2012", "Sudoku", "Sjakk" eller BÅDE "Numpy-arrays og matplotlib" OG "Bokanalyse med plotting". Hvis man gjør mer enn én, vil dette tilsvare to øvinger godkjent. Sjakk-oppgaven teller som to øvinger.

**Begge:** Man kan maksimalt få to godkjente øvinger ved å gjøre øving 10. Oppgaver merket med en fagkode i parantes er kun relevant for denne fagkodens pensum, og kan ignoreres av andre.

Oppgavene er markert med vanskelighetsgrad.

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!


Oppgave | Tema | Vanskelighetsgrad
--- | --- | ---
[Rekursjon](Lett/Rekursjon.md) (Obligatorisk TDT4109)| Rekursjon, Algoritmer (TDT4109)| Lett
[Matplotlib](Lett/Plotting.md) (Obligatorisk TDT4110)|Plotting (TDT4110)| Lett
[Eksamen Python 2012](Lett/Eksamen%202012.md)|Repetisjon| Lett
[Sudoku](Lett/Sudoku.md)|Repetisjon, Sammensatte problemer| Lett
[Numpy arrays og matplotlib](Lett/Numpy-arrays.md) (TDT4110)|Numpy arrays, Plotting (TDT4110)| Lett
[Bokanalyse med plotting](Middels/Bokanalyse.md) (TDT4110)|Filhåndtering, Plotting, Funksjoner, Strenghåndtering|Middels
[Sjakk](Vanskelig/Sjakk.md)|Repetisjon, Sammensatte problemer|Vanskelig