### **[Intro øving 8](../Intro_Øving8.md)**

<br>

# Bokanalyse med plotting (TDT4110)

I denne oppgaven skal du analysere boken *Alice in Wonderland* og se hvor mange ganger forskjellige ord dukker opp i de forskjellige kapitlene. Dette skal vi etterhvert plotte inn i en graf ved hjelp av `matplotlib`.

Filen vi skal bruke er [alice_in_wonderland.txt](../../Oving7/Middels/alice_in_wonderland.txt), som er hele den engelske versjonen av Alice i eventyrland i en tekstfil. Den samme filen som vi brukte i øving 7 ["Søke i tekst"](../../oving7/../Oving7/Middels/Søke%20i%20tekst.md).


## OPPGAVER
Alle deloppgaver skal besvares her: *[Bokanalyse.py](Bokanalyse.py)* !

<br>

## a)
I første oppgave skal du lage funksjonen `get_chapters(filename, chapter_delimiter)` som tar inn et filnavn `filename` pluss en *skillestreng* `chapter_delimiter` og returnerer en liste av strenger hvor hver streng er teksten til et kapittel i boken. Skillestrengen er en streng som befinner seg før hvert kapittel, men *ikke noe annet sted i boken*.

Forordet skal ikke være med, så den første strengen i listen som returneres skal være teksten til kapittel 1. 

I alice_in_wonderland.txt finnes strengen `'CHAPTER'` (i store bokstaver) mellom hvert kapittel, så til denne boka kan vi bruke `'CHAPTER'` som vår `chapter_delimiter`.

Hvis du vil teste koden din så kan du sjekke at for alice_in_wonderland så skal den returnerte listen ha lengde 12.


<br>

## b)
Lag funksjonen `count_words(string_list, word)` som tar inn en liste med strenger `string_list` og et ord `word` og returnerer en ny liste med antallet ganger `word` befinner seg i hver av strengene i `string_list`. Se på eksempelet lenger ned hvis dette var uklart.

Funksjonen skal gjelde uavhengig av hvilke bokstaver som er store og små i `word` og `string_list`. 

<br>

## c)
I den neste oppgaven skal du lage funksjonen `create_numbers_to(number)` som tar inn et nummer `number` og returnerer en liste som inneholder like mange elementer som `number` på formen `[1,2,3,4..., number-1, number]`. Det høyeste (og siste) tallet i listen skal altså være `number`

For eksempel skal `create_numbers_to(7)` returnere listen `[1, 2, 3, 4, 5, 6, 7]`

<br>

## d)
Det er nå dette virkelig blir gøy, for nå skal du lage funksjonen `analyze_book(filename, chapter_delimiter, word)` som tar i bruk funksjonene du har definert i de forrige oppgavene. `analyze_book` tar inn ordet `word` og plotter en graf hvor x-aksen er kapittelnumrene i boka, mens y-aksen er antall ganger `word` finnes i dette kapittelet. `filename` og `chapter_delimiter` er det samme som i oppgave a (og trengs nesten kun for å sendes inn i den funksjonen, hint hint). Hvis du ikke husker hvordan du plotter grafer kan du se på de tidligere øvingsoppgavene. Grafen skal se ut som dette:

- Tittelen på grafen skal være: Antall ganger "<word\>" dukker opp per kapittel i "<filename\>"'
- Tittelen på x-aksen skal være: Kapittel
- Tittelen på y-aksen skal være: Antall "<word\>"
- x-aksen skal gå fra 1 til antall kapittel i boka
- y-aksen skal gå fra 0 til det høyeste antallet ganger `word` dukker opp i et kapittel + 3. (så grafen ikke krasjer i taket)

<word\> må her byttes ut med ordet som blir sendt inn i funksjonen, og <filename\> må byttes ut med filnavnet som blir sendt inn i funksjonen.

<br>

**Hint**<br>
Du kan bruke funksjonen i oppgave a for å dele opp teksten i kapitler, funksjonen i oppgave b for å finne y-verdiene og funksjonen i oppgave c for å finne x-verdiene.

Det er mulig at du kan få bruk for den innebygde funksjonen `max(liste)` som finner det høyeste tallet i listen.

<br>

## e)
Lag funksjonen `analyze_multiple_words(filename, chapter_delimiter, words)`. Her er `words` en liste med ord, og funksjonen skal tegne en graf med `analyze_book` for hvert av ordene i `words`.

<br>


