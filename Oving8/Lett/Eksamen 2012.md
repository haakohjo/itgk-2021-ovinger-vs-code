### **[Intro øving 8](../Intro_Øving8.md)**

<br>

# Eksamen 2012

***I denne versjonen har vi fjernet fjervalgsoppgavene som til vanlig teller 25% av eksamen. Topp prosentscore er da altså 75%.***

<br>

## OPPGAVER
Alle oppgaver løses her: *[Eksamen 2012.py](Eksamen%202012.py)* !

## 2 - grunnleggende programmering

## a) (5%)
Lag funksjonen **summerOlympics** som har inn-parametere **firstYear** og **lastYear**. Funksjonen skal returnere variabelen **years**,som er ei liste med alle OL-årene fra og med **firstYear** til og med **lastYear** (inkludert framtidige planlagte år for sommer-OL). Fra og med OL i London i 1948,har sommer-OL vært arrangert hvert fjerde år. Du kan anta at **firstYear** ≥ 1948.

Eksempel på kjøring av funksjonen og hva den returnerer:

```python
years = summerOlympics(1999,2012)
print(years)
```

$->[2000, 2004, 2008, 2012]$


## b) (7.5%)
Lag funksjonen **`findAge`** som har inn-parametere **bYear**, **bMonth**, **bDay** som er tre heltall som beskriver dato for en fødselsdag. Funksjonen skal returnere **age** som beskriver hvor gammel en person med oppgitt fødselsdag (**bYear**, **bMonth** og **bDay**) er i dag angitt i hele år.

For å finne år, måned og dag for i dag ***skal du bruke*** en eksisterende funksjon som heter `current_date()`.Funksjonen returnerer tre heltall på formatet **(yyyy,mm,dd)**. 

Eksempel på bruk av funksjonen **current_date**:  
`(yyyy,mm,dd)= current_date()` gir i dag yyyy=2012,mm=12, dd=11.

Eksempel på kjøring av funksjonen **`findAge`** og hva den returnerer:

```python
age = findAge(2000,12,15)
print(age)
```
$-> 11$

<br>

## c) (7.5%)
Lag en funksjon **`printAgeDiff`** som tar en parameter **table**,som er en to-dimensjonal tabell (liste av lister) der hver rekke beskriver personer med fornavn, etternavn, fødselsår, fødselsmåned og fødselsdato. Funksjonen skal bruke funksjonen **`findAge`** fra oppgave 2b til å sammenlikne alderen i hele år på etterfølgende personer i tabellen (rekke for rekke) og gjøre følgende:

* Hvis person n og person n+1 har samme alder angitt i antall hele år, skal følgende skrives ut til skjerm:  
``<fornavn n> <etternavn n> is at the same age as <fornavn n+1> <etternavn n+1>``
* Hvis person n er eldre enn person n+1 angitt i antall hele år, skal følgende skrives ut til skjerm:  
``<fornavn n> <etternavn n> is older than <fornavn n+1> <etternavn n+1>``
* Hvis person n er yngre enn person n+1 angitt i antall hele år, skal følgende skrives ut til skjerm:  
``<fornavn n> <etternavn n> is younger than <fornavn n+1> <etternavn n+1>``

Eksempel på en to-dimensjonal tabell som beskriver fire kjente personer:

```python
table=[['Justin','Bieber',1994,3,1],
       ['Donald','Duck',1934,8,1],
       ['George','Clooney',1961,5,6],
       ['Eddie','Murphy',1961,4,3]]
```
Eksempel på kjøring av funksjonen **`printAgeDiff`** med tabellen **table**, som inneholder listene for bieber, donald, george og eddie:

```python
printAgeDiff(table)
```
$->$ *Justin Bieber is younger than Donald Duck <br>
Donald Duck is older than George Clooney <br>
George Clooney is at the same age as Eddie Murphy*

<br>

## Oppgave 3 - Kodeforståelse
## a) (5%)

Hva returneres hvis funksjonen `fu1(1234)`med kode som vist under kjøres, forklar hva som skjer med en setning?

```python
def fu1(a):
    r = 0
    while(a>0):
        s = a%10
        r = r + s
        a = (a-s)/10
return r
```
<br>
<br>

## b) (5%)
Hva blir verdiene til a,b,c og d etter kallet  
`(a, b, c, d) = fu2(’Ut pa tur, aldri sur’)`  
med koden som vist under?

```python
def fu2(input):
    r = 0
    s = 0
    t = 0
    u = 0
    n = len(input)
    for c in input:
        if(c.isalpha()):
            r = r + 1
        elif(c.isdigit()):
            s = s + 1
        elif(c==' '):
            t = t + 1
        else:
            u = u + 1
    r = 100*r/n
    s = 100*s/n
    t = 100*t/n
    u = 100*u/n
    return(r,s,t,u)
```

<br>

## c) (5%)
Hva returneres av kallet `fu3(100)`med koden som vist under?
```python
def fu3(a):
    if(a<=2):
        r = 1
    else:
        r = 1 + fu3(a/2)
    return r
```

<br>

## Oppgave 4 - Programmering
Denne oppgaven fokuserer på behandling av data fra fire vær sensorer som måler en verdi per døgn av følgende data:

* Temperatur: Angis som heltall i Celsius fra -50C til + 50C
* Nedbør: Angis som heltall i mm nedbør per døgn fra 0 til 2000mm
* Luftfuktighet. Angis som heltall fra 0 til 100%
* Vindstyrke: Angis som heltall fra 0 til 50 meter per sekund

Hvis ikke noe annet er oppgitt kan du anta korrekt input til funksjonene.

<br>

## a) (5%)
Lag en funksjon **`cold_days`** som tar imot parameteren **templist**, som en liste av temperaturer, og returnerer variabelen **days**,som angir antall døgn der temperaturen var under 0 grader.

Eksempel på kall av funksjonen og hva den returnerer:
```python
days = cold_days([1,-5,3,0,-6,-3,15,0])
print(days)
```
$->3$

<br>

## b) (5%)
Lag en funksjon **`cap_data`** som har inn-parameterne **array** (liste med data), **min_value** (minimumsverdi) og **max_value** (maksimumsverdi). Funksjonen skal returnere ei ny liste **result** der alle elementer i lista **array** som har verdi mindre enn **min_value** skal settes lik **min_value** og alle elementer i lista **array** som har verdi høyere enn **max_value** skal settes lik **max_value**.

Eksempel på kall av funksjonen og hva den returnerer:
```python
A=[-70,30,0,90,23,-12,95,12]
result = cap_data(A,-50,50)
print(result)
```
$->[-50,30,0,50,23,-12,50,12]$


Legg merke til hvilke verdier som endres.


<br>

## c) (10%)
Lag en funksjon **`generate_testdata`** som har inn-parameterne **N**, **min_value** (minimumsverdi)og **max_value** (maksimumsverdi). Funksjonen skal returnere tabellen **result** som består av **N** unike *tall* (heltall) som blir trukket tilfeldig der {**min_value** ≤ *tall* ≤ **max_value**}. Unik betyr her at ingen elementer i tabellen **result** skal ha samme verdi. Du kan anta at antall mulige verdier i intervallet tallet blir trukket fra alltid vil være større enn **N**.

Eksempel på kall av funksjonen og hva den returnerer:
```python
result = generate_testdata(10,-5,10)
print(result)
```
$->[-5,3,7,9,-3,4,2,0,-1,5]$

<br>

## d) (5%)
Lag en funksjon **`create_db`** som har inn-parameterne **temp**, **rain**, **humidity** og **wind**,som er fire tabeller av samme størrelse (likt antall elementer) med data for temperatur, nedbør, luftfuktighet og vind.

Funksjonen skal lage og returnere *dictionarien* **weather**,der nøkkelen er ett heltall som starter med verdien 1 og teller oppover (representerer dagen for måling). Hvert innslag i *dictionarien* skal være en liste av verdier for temperatur, nedbør, luftfuktighet og vind. Verdiene for **weather** med nøkkel 1 skal inneholde værdata for dag 1, **weather** med nøkkel2 skal inneholde værdata for dag 2 og så videre.

Eksempel på kall av funksjonen og hva den returnerer:
```python
temp = [1,5,3]
rain = [0,30,120]
humidity = [30,50,65]
wind = [3,5,7]
weather = create_db(temp,rain,humidity,wind)
print(weather)
```
$->$ {$1: [1, 0, 30, 3], 2: [5, 30, 50, 5], 3: [3, 120, 65, 7]$}

<br>

## e) (5%)
Lag en funksjon **`print_db`** som har inn-parameteren **weather**,som er en dictionary som beskrevet i oppgave 4d. Funksjonen skal skrive ut innholdet i **weather** på skjerm etter følgende format og med overskrift som vist på utskriften nederst i deloppgaven:
* Day(dag) –høyrejustert med 4 tegn
* Temp (temperatur)–høyrejustert med 6 tegn
* Rain (nedbør)–høyrejustert med 6 tegn
* Humidity (luftfuktighet)–høyrejustert med 10 tegn
* Wind (vind)–høyrejustert med 6 tegn

Eksempel på kall av funksjonen ved bruk av dictionarien fra oppgave 4d:
```py
print_db(weather)
```
$->$
```
Day | Temp | rain | humidity | wind
====+======+======+==========+======
   1      1      0         30      5
   2      5     30         50      3
   3      3    120         65      7

```

<br>

## f) (10%)
Lag funksjonen **`strange_weather`** som har inn-parameterne **temp** og **rain**,som er to tabeller med data for temperaturer og regn av lik størrelse (samme antall elementer). Funksjonen skal returnere **start** (startdag)og **stop** (sluttdag)for det lengste intervallet der det er minusgrader, samt at temperaturen faller samtidig som nedbørsmengden stiger i etterfølgende dager. Indekseringen av dager starter på 1. Hvis ingen etterfølgende dager har denne karakteristikken, returneres (0,0).

Eksempel på kall av funksjonen(med intervall som oppfyller kravet uthevet):
```python
temp=[1,3, 4,-5,-6,-7,-8,-9,3,0]
rain=[0,20,30,0,10,30,50,0,5,2]
(start, stop) = strange_weather(temp,rain)
print(start," - ",stop)
```
$-> 4 - 7$
