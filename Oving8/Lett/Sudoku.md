### **[Intro øving 8](../Intro_Øving8.md)**

<br>

# Sudoku

**Læringsmål.**

* Store problemstillinger


## OPPGAVE
Oppgaven skal løses her: *[Sudoku.py](Sudoku.py)* !  


I denne oppgaven skal vi først lage et sudoku-spill (før vi lager en sudoku-løser om noen år). Om du ikke kjenner reglene til sudoku kan du lese deg opp på de selv [her](https://no.wikipedia.org/wiki/Sudoku). Del 1 tar seg av å lage et spillbart sudoku-brett.

Du står fritt til å bygge opp brettet slik som du vil, men brettet skal oppfylle følgende krav:

* Brukeren skal kunne skrive inn et tall i en valgfri celle. Dersom tallet ikke er gyldig, dvs. ikke mellom 1 og 9, skal en feilmelding skrives ut.
* Brukeren skal ikke kunne fylle inn et tall som allerede finnes i den samme raden, kolonnen eller kvadratet.
* Brukeren skal kunne slette et tall fra en celle.
* Hver gang brukeren fyller inn eller sletter et tall skal det nye brettet skrives ut på en fin måte. Et eksempel kan være som vist under (tallene over og ved siden av brettet angir her henholdsvis kolonne- og radnummer).
* Brukeren skal kunne laste inn et brett fra en tekstfil.
* Et halvutfylt brett skal kunne lagres til fil, slik at man kan fullføre det senere.
* Spillet skal skrive ut en hyggelig gratulasjonsmelding dersom man har klart brettet.
* Alt skal utføres gjennom et brukervennlig grensesnitt. Det vil si at brukeren ikke skal trenge å kalle på funksjonene selv, men at alt gjøres via input eller ved å lese fra/skrive til fil.

Eksempel på brett:
```python
    0 1 2   3 4 5   6 7 8 
  +-------+-------+-------+
0 | 0 0 6 | 9 0 5 | 0 1 0 |
1 | 9 7 0 | 0 1 2 | 3 0 5 |
2 | 0 2 0 | 0 0 4 | 8 6 0 |
  +-------+-------+-------+
3 | 5 0 3 | 8 0 0 | 0 2 0 |
4 | 0 0 0 | 0 0 0 | 0 0 0 |
5 | 0 8 0 | 0 0 1 | 9 0 7 |
  +-------+-------+-------+
6 | 0 5 4 | 1 0 0 | 0 7 0 |
7 | 2 0 7 | 4 5 0 | 0 9 3 |
8 | 0 6 0 | 7 0 3 | 1 0 0 |
  +-------+-------+-------+
```