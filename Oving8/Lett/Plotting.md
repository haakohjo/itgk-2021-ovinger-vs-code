### **[Intro øving 8](../Intro_Øving8.md)**

<br>

# Grunnleggende om plotting (Obligatorisk TDT4110)

**Læringsmål:**
- Plotting


<br>

## INTRODUKSJON

For å tegne grafer er vi nødt til å bruke ekstrafunksjoner som er utenfor "standard python". Disse funksjonene er lagret i såkalte *bibliotek*, som er kode andre har laget og som vi kan bruke. Biblioteket vi skal bruke for å plotte grafer heter `matplotlib`.

Ofte er det ikke nødvendig å vite alt om hvordan et bibliotek fungerer, men heller hvordan man bruker det. Dette kan man finne ut av ved for eksempel å google eksempler på bruk (tutorials) eller lese dokumentasjonen for biblioteket. Den offisielle oversikten over matplotlib (fra de som har laget det) ligger for eksempel [her](https://matplotlib.org/3.1.1/contents.html). Denne inneholder informasjon om alt det er mulig å bruke matplotlib til (som er veldig mye!) og trengs ikke å leses for å kunne følge med videre i dette dokumentet.

### **Importering av matplotlib**

For å kunne bruke biblioteket `matplotlib` må vi først *importere* det. I vårt tilfelle trenger vi ikke å importere hele matplotlib, men bare delen `pyplot` som er den delen som lar oss tegne grafer. Denne importerer vi i koden under og kaller for `plt`. Dette gjør at vi senere kun trenger å skrive `plt` i stedet for `matplotlib.pyplot`.

```python
import matplotlib.pyplot as plt
```

<br>

### **Grafer**
Under ser du et eksempel på en enkel kode som tegner en graf. Dette eksempelet består av tre kodelinjer som alle bruker `pyplot` fra `matplotlib`-biblioteket (som vi har døpt om til `plt`)

- Funksjonen `plt.plot` tar inn en liste med verdier som blir y-verdiene i grafen.
- Funksjonen `plt.ylabel` brukes for å sette teksten som skal være på y-aksen.
- Funksjonen `plt.show` brukes for å vise grafen

```python
plt.plot([1, 2, 4, 3])
plt.ylabel('Noen tall')
plt.show()
```
Det verdt å merke seg at listen som `plt.plot` tar inn også kan være en variabel. Dette er gjort i eksemepelet under. Her spørr vi brukeren om 4 tall og plotter disse tallene inn i en graf.

```python
numbers = []
for x in range(4):
    number = int(input("Skriv inn et tall: "))
    numbers.append(number)

plt.plot(numbers)
plt.ylabel('Tallene du skrev inn')
plt.show()
```
Det virker kanskje som om `plt.plot` ikke er så nyttig, siden alle punktene kun har 1 mellom seg på x-aksen. Dette er feil. Funksjonen har faktisk et hav av muligheter! Vi kan for eksempel spesifisere punkter både på x-aksen og y-aksen ved å gi inn to lister til funksjonen. I tillegg kan vi bruke funksjonen `plt.axis` for å bestemme hva som skal være startverdi og sluttverdi for aksene.

```python
x_verdier = [2,4,8,16]
y_verdier = [4,2,9,3]
plt.plot(x_verdier, y_verdier)
plt.axis([0, 20, 0, 15]) # x-aksen går her fra 0 til 20, mens y-aksen går fra 0 til 15
plt.show()
```

Det er også mulig å lage punkter i stedet for linjer. Her må det legges med en streng i et spesielt format til `plt.plot`. I eksempelet under betyr strengen `"ro"` at vi bruker røde prikker. `r` for *red*, `o` for *runding*.

```python
plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'ro')
plt.axis([0, 6, 0, 20])
plt.show()
```
Dette gjør at vi også kan plotte flere grafer oppå hverandre og skille mellom de, som i eksempelet under:
```python
plt.plot([0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], 'r-')
plt.plot([0, 1, 2, 3, 4, 5], [0, 1, 4, 9, 16, 25], 'b-')
plt.axis([0, 6, 0, 26])
plt.show()
```


<br>

<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Plotting.py](Plotting.py)* !

I denne oppgaven skal du plotte en sinus-funksjon. Vi kan ikke plotte funksjonen direkte uten videre som vi ville gjort i geogebra, så vi må i stedet lage et stort antall punkter og plotte disse punktene. Vi gjør dette et steg av gangen.

## a)
Lag en liste som heter `x_verdier` som inneholder alle verdiene fra og med `0.0` til og med `30.0` med et mellomrom på `0.1`. Print deretter ut x-verdiene. Har du gjort alt riktig skal utskriften være:


$->[0.0, 0.1, 0.2, ... , 29.8, 29.9, 30.0]$

<br>

## b)
Nå skal du lage en ny liste som heter `y_verdier`. Denne skal inneholde `sin(x)` for alle x-verdiene i `x_verdier`. 

For å ta sinus av et tall kan du importere biblioteket `math`, for så å bruke funksjonen `math.sin` som her:
``` python
import math
math.sin(3) # Vil returnere sin(3), hvor 3 er en verdi i radianer
```

Når du har laget `y_verdier` printer du listen. Hvis du har gjort alt riktig skal utskriften være:

$->[[0.0, 0.09983341664682815, 0.19866933079506122, ... , -0.9989818049469494, -0.9984950306638146, -0.9880316240928618]$

<br>

## c)
Du skal nå plotte punktene du har definert i oppgave a og oppgave b. Grafen skal lages med `x_verdier` som x-verdier, `y_verdier` som y-verdier og være en rød linje. Hvis du lurer på hvordan du tegner grafer, les introduksjonen.


