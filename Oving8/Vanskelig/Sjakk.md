### **[Intro øving 8](../Intro_Øving8.md)**

<br>

# Sjakk

**Læringsmål:**

* Store problemstillinger

<br>

## OPPGAVE
Oppgaven skal løses her: *[Sjakk.py](Sjakk.py)* !

I denne oppgaven skal du implementere sjakk. Begynn med den så fort som mulig, og jobb med den kontinuerlig (les: denne oppgaven krever mye arbeid).

Bruk læringsassistent på sal når du har muligheten.

Under er forslag til fremgangsmåte, men du kan gjøre det som du vil. Det som kreves av programmet er:

1. Programmet må kunne ta inn trekk fra spillerne (tekstbasert).
2. Programmet skal hindre spillerne i å gjøre ulovlige trekk.*
3. Programmet skal oppdage og si ifra når en spiller er i sjakk.
4. Programmet skal oppdage og si ifra når det er sjakkmatt.
5. Hvis en bonde når andre siden av brettet skal den bli forfremmet til en offiser (dronning, løper, springer eller tårn).

**Du trenger IKKE ta hensyn til patt, remis, en passant, rokering eller tidskontroll.**

Regler for sjakk og hvordan brikkene kan bevege seg finnes her: http://no.wikipedia.org/wiki/Sjakk

**Forslag til framgangsmåte:**

Her finner du et forslag til fremgangsmåte, den er veiledende, men kan være til hjelp hvis du ikke helt vet hvor du skal begynne.

**a)** Velg en måte å representere et sjakkbrett. Vi anbefaler en todimensjonal liste. Velg også hvordan du skal representere en tom rute og at en rute er opptatt av en bestemt brikke.

 

**b)** Initialiser brettet med alle brikkene i sin korrekte startposisjon.

 

**c)** Gjør det mulig å se brikkene på skjermen. Med andre ord skriv en funksjon som printer ut brettet fint til skjermen.

For et penere design, kopier og lim inn disse symbolene i koden din: ♖, ♜, ♗, ♝, ♘, ♞, ♕, ♛, ♔, ♚, ♙, ♟

 

**d)** Gjør det mulig for en spiller å flytte på en brikke. Hvordan kan du implementere at en brikke tar en annen?

 

**e)** Legg til funksjonalitet som sjekker om trekket er lovlig. Gjør dette i flere omganger:

1. Er trekket av en gyldig form? (’L’ fasong for springer, diagonalt for løper etc.)
2. Er trekket blokkert av en annen brikke?
3. Pass på at det ikke er mulig å ta en brikke av din egen farge.
 

**f)** Skriv funksjonalitet som tester om kongen er i sjakk. Husk også at det er et ulovlig trekk å sette sin egen konge i sjakk, utvid testene over slik at dette også blir et ugyldig trekk.

 

**g)** Skriv funksjonalitet for å sjekke om kongen er i sjakk-matt. (Her kan du gjenbruke kode fra sjakk-testen)

 

**h)** Skriv funksjonalitet som oppdager at en bonde er flyttet til andre siden av brettet og la spilleren forfremme bonden til en dronning/tårn/løper/springer.