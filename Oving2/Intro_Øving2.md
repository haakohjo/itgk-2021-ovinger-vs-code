# Øving 2

**Læringsmål:**

- If-setninger, hvorfor og hvordan
- Betingelser, Logiske uttrykk
- IT/hardware i hverdagen

**Starting Out with Python:**

- Kap 3 - Decision Structures and Boolean Logic

**Theory book**

- Kap 1 - Defining information Technology
- Kap 7 - Representing Information Digitally
- Kap 9 - Principles of Computer Operations

<br>

## Godkjenning:
For å få godkjent øvingen må du gjøre **teorioppgaven** og **4 av 11** andre oppgaver.
Oppgavene er markert med vanskelighetsgrad. 

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!

Oppgave | Tema | Vanskelighetsgrad
--- | --- | ---
[Ulike typer if-setninger](Lett/Ulike%20typer%20if-setninger.md) | Valg, beslutninger og betingelser. | Lett
[Sammenligning av strenger](Lett/Sammenligning%20av%20strenger.md) | Betingelser |Lett
[Logiske operatorer og uttrykk](Lett/Logiske%20operatorer%20og%20uttrykk.md) |Betingelser, logiske uttrykk | Lett
[Forbrytelser og straff](Lett/Forbrytelser%20og%20straff.md) |Kodeforståelse, feilretting | Lett
[Årstider](Lett/Årstider.py) | Betingelser, løgiske uttrykk | Lett
[Tekstbasert spill](Middels/Tekstbasert%20spill.md) | Betingelser | Middels 
[Sjakkbrett](Middels/Sjakkbrett.md) | Betingelser, logiske uttrykk | Middels
[Billettpriser og rabatter](Middels/Billettpriser%20og%20rabatter.md) |Betingelser | Middels
[Skatteetaten](Middels/Skatteetaten.md) | Betingelser | Middels
[Epletigging](Middels/Epletigging.md) | Feilretting, betingelser | Middels
[Andregradsligning](Middels/Andregradsligning.md) | Betingelser, avrundingsfeil | Middels





