### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Skatteetaten

**Læringsmål:**

* Betingelser

**Starting Out with Python:**

* Kap. 3.1- 3.2
* Kap. 3.4
 

I denne oppgaven skal du lage et program som tar inn opplysninger om utleie av eiendom fra en bruker. Programmet vil så beregne hvor stor andel av inntekten som er skattbar.  

Regler for skatt finnes på Skatteetaten sine hjemmesider, men er ikke nødvendig å sette seg inn i: 

http://www.skatteetaten.no/no/Person/Selvangivelse/tema-og-fradrag/Jobb-og-utdanning/delingsokonomi/utleie-av-bolig-og-fritidsbolig/

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Skatteetaten.py](Skatteetaten.py)* !

## a)
Lag et program som ber brukeren om opplysninger og svarer om inntekten er skattepliktig eller skattefri. 

Regler som må implementeres:

* Hvis du bruker minst halvparten av boligen du eier til eget bruk, beregnet etter utleieverdi, er det skattefritt å leie ut resten.
*  Leier du ut mer enn halvparten av egen bolig, men for under 20 000 kr i året er det også skattefritt.
* Leier du ut hele eller mer enn halvparten av egen bolig for over 20 000 kr i året er samtlige leieinntekter for hele året skattepliktige. 

**Eksempel på kjøring av kode:**
 

INFO  
*Dette programmet besvarer om din utleie av egen bolig er skattepliktig. 
Først trenger vi å vite hvor stor del av boligen du har leid ut.
Angi dette i prosent, 100 betyr hele boligen, 50 betyr halve,
20 en mindre del av boligen som f.eks. en hybel.*

DATAINNHENTING: <br> 
*Oppgi hvor mye av boligen som ble utleid:* 65  <br>
*Skriv inn hva du har hatt i leieinntekt:* 45000   
 
SKATTEBEREGNING:  
*Inntekten er skattepliktig  
Skattepliktig beløp er 45000*

<br>

## b)
For å leie ut *sekundærbolig* eller *fritidsbolig* gjelder det særskilte regler. Lag et tilsvarende program som dekker disse behovene. (Se samme nettside for mer informasjon om ønskelig.)

Regler som må implementeres:

* Sekundærbolig:
 * Utleie av sekundærbolig beskattes fra første krone.
* Fritidsbolig:
 * Der du helt eller delvis bruker fritidsboligen til fritidsformål, og selv bruker eiendommen i rimelig omfang over tid, så vil utleieinntekter inntil kr 10 000 være skattefrie.
 * Av det overskytende beløp regnes 85 prosent som skattepliktig inntekt.
 * Dersom fritidsboligen anses som utleiehytte blir det beskatning fra første krone.
 * Om du leier ut flere enn en fritidsbolig vil grensen på 10 000 gjelde per fritidsbolig.
 
**Eksempel på kjøring av kode:**
 

INFO <br>
*Dette programmet besvarer om din utleie en annen type bolig,
her sekundær- eller fritidsbolig, er skattepliktig.
Først trenger vi å vite om du leier ut en sekundær- eller en fritidsbolig.*

DATAINNHENTING: <br>
*Skriv inn type annen bolig (sekundærbolig/fritidsbolig) du har leid ut:* Fritidsbolig
    
INFO <br>
*Du har valgt fritidsbolig.
Nå trenger vi først å vite om fritidsboligen(e) primært brukes til utleie eller fritid. <br>
Deretter trenger vi å vite hvor mange fritidsbolig(er) du leier ut. <br>
Til slutt trenger vi å vite hvor store utleieinntekter du har pr. fritidsbolig.*


DATAINNHENTING: <br>
*Skriv inn formålet med fritidsboligen(e):* Fritid
*Skriv inn antallet fritidsboliger du leier ut:* 3
*Skriv inn utleieinntekten pr. fritidsbolig:* 15000
    

SKATTEBEREGNING: <br>
*Inntekten er skattepliktig <br>
Overskytende beløp pr. fritidsbolig er 5000 <br>
Skattepliktig inntekt pr. fritidsbolig er 4250 <br>
Totalt skattepliktig beløp er 12750*


<br>

## c)
Sett sammen del (a) og (b) til et større program som først spør brukeren hva som er blitt leid ut (egen bolig / sekundærbolig / fritidsbolig), og deretter regner ut passende skattesats. Du kan delvis kopiere koden fra de tidligere deloppgavene.