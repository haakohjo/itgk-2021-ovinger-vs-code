### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Billettpriser og rabatter

**Læringsmål:**

* Betingelser

**Starting Out with Python:**

* Kap 3.1, 3.2, 3.4

I denne oppgaven skal du bruke betingelser (if-elif-else) til å bestemme billettprisen til en bruker basert på input fra brukeren. 

Transportselskapet «TravelevatoR» tilbyr reiser fra Trondheim til Pythonville. Ved bestilling minst 14 dager før man skal reise, kan man få minipris 199. Normal billett koster 440. 


## OPPGAVER
Alle deloppgaver skal besvares her: *[Billettpriser og rabatter.py](Billettpriser%20og%20rabatter.py)* !

## a)
Skriv et program med to mulige oppførsler som vist under:

<br>

*Dager til du skal reise?* 15  
*Du kan få minipris: 199,-*  
___
*Dager til du skal reise?* 8  
*For sent for minipris; fullpris 440,-*

<br>

## b)
Det hender av og til at kunder ikke ønsker minipris, men heller vil ha fullprisbillett. Dette fordi minipris ikke kan refunderes / endres. Utvid derfor programmet så kunder spørres om de ønsker minipris – men kun der det er relevant. To eksempel på kjøring etter utvidelsen:

<br>

*Dager til du skal reise?* 17 <br>
*Minipris 199,- kan ikke refunderes/endres* <br>
*Ønskes dette (J/N)?* J <br>
*Takk for pengene, god reise!*
___
*Dager til du skal reise?* 17 <br>
*Minipris 199,- kan ikke refunderes/endres <br>
Ønskes dette (J/N)?* N <br>
*Da tilbyr vi fullpris: 440,-*

<br>

## c)
Det blir besluttet at barn under 16 gis 50% rabatt. Senior (60+), militær i uniform, og student gis 25% rabatt. Om man f.eks. både er 60+ og militær / student, får man likevel bare 25% rabatt. Alle disse rabattene gjelder kun i forhold til fullpris, for minipris gis ingen rabatter. Endre programmet så det spør kunden om alder, evt. også om student / militær, og regner ut riktig pris.

**NB:** De nye spørsmålene skal ***kun*** stilles når de er relevante, da det er et mål at kunden skal måtte svare på så få spørsmål som mulig.

Eksempel på kjøring av koden:

  
*Dager til du skal reise?* 17 <br>
*Minipris 199,- kan ikke refunderes/endres <br>
Ønskes dette (J/N)?* N <br>
*Skriv inn din alder:* 15 <br>
*Prisen på biletten blir:* 220,-


<br>

## d) Valgfri ekstraoppgave
Du får godkjent oppgaven om du bare har klart a, b og delvis c, men prøv gjerne på denne litt vanskeligere d også.

Etter lobbyvirksomhet fra studentorganisasjoner endres reglene på følgende måte: Studenter får også litt rabatt på miniprisbilletter (10%),  samt at personer som både er studenter og militære  / 60+, eller studenter og barn, får to rabatter, og dermed bare betaler 0,75 * 0,75 eller 0.75 * 0.5 av fullpris. Merk at personer som er 60+ og militære, men ikke studenter, eller barn og militære, men ikke studenter, ikke får noen slik multiplisert rabatt, de må fortsatt nøye seg med den ene rabatten de hadde før, og de vanlige 199 for minipris. Endre programmet så det fungerer korrekt i forhold til de nye reglene.