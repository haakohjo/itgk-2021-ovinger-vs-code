### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Ulike typer if-setninger

**Læringsmål:**

- Betingelser

**Starting Out with Python:**

- Kap. 3.1- 3.2
- Kap. 3.4


## INTRODUKSJON

## **if-setninger**

I oppgavene i øving 1 har det vært slik at alle setningene i programmet skulle utføres hver gang programmet kjørte. De fleste nyttige programmer er ikke så enkle. Ofte skal visse programsetninger kun utføres under gitte betingelser. Overtidslønn skal utbetales bare hvis det har vært jobbet overtid. Monsteret du kjemper mot i et spill, skal miste liv bare hvis skuddet eller sverdslaget ditt har truffet. En alarm skal aktiveres bare hvis pasientens hjerterytme er blitt unormal. En sosial app på mobilen skal gi et spesielt varsel bare hvis du har fått nye meldinger der. Kodeblokken nedenfor viser et eksempel hvor alle linjene blir utført - dette programmet vil anbefale paraply uansett hvordan været er.

```python
regn = float(input("Hvor mange mm regn er det meldt? "))
print("Da anbefaler jeg paraply!")
print("Ha en fin tur til universitetet!")
```
Paraply bør anbefales bare ved behov, f.eks. hvis det var meldt mer enn 0.2 mm regn. Det oppnår vi med en if-setning:

```python
regn = float(input("Hvor mange mm regn er det meldt? "))
if regn > 0.2:
    print("Da anbefaler jeg paraply!")
print("Ha en fin tur til universitetet!")
```


Den ovenstående kodeblokken viser en if-setning. Den har følgende struktur:

- starter med ordet `if`, dette er et beskyttet ord i Python og kan derfor ikke brukes som variabelnavn e.l.
- bak if kommer en logisk betingelse - i dette tilfellet `regn > 0.2` - som vil være enten sann (`True`) eller usann (`False`), her avhengig av innholdet i variabelen regn
- bak betingelsen **må** if-setningen ha `:` **(kolon)** - hvis du glemmer kolon vil du få syntaksfeil. 
- kodelinjen like under if har et innrykk (tabulator), dette betyr at denne linjen - `print("Da anbefaler jeg paraply")` - er del av if-setningen. Den vil bli utført **kun hvis betingelsen i if-setningen er sann**, som vil inntreffe hvis brukeren skriver inn et større tall enn 0.2

Siste setning, `print("Ha en fin tur til universitetet!")` , har derimot ikke innrykk, denne vil derfor bli utført uansett og er ikke knyttet til if-setningen.

Det er mulig å ha flere programsetninger som del av if-setningen, for eksempel:

```python
regn = float(input("Hvor mange mm regn er det meldt? "))
if regn > 0.2:
    print("Da anbefaler jeg paraply!")
    print("Gjerne en paraply med NTNU-logo.")
print("Ha en fin tur til universitetet!")
````
Her vil begge "print"-setningene om paraply kun bli utført hvis betingelsen er sann, mens "fin tur"-setningen fortsatt blir utført uansett utfall av if-setningen.

**Altså: I Python viser innrykk hvor mange av de påfølgende setningene som styres av if-betingelsen.** Det er derfor viktig å være nøye med innrykkene og få alle programsetninger på korrekt indentasjonsnivå.

<Br>

## **if-else-setninger**

I eksemplene over skulle vi gjøre noe ekstra hvis en betingelse var sann, ellers la være. I andre tilfeller kan vi ønske å gjøre en handling hvis betingelsen er sann, og en alternativ handling hvis den er usann.

Det kan vi oppnå med en **if-else**-setning. Eksemplet nedenfor bygger videre på tilsvarende eksempel for if-setninger.

```python
regn = float(input("Hvor mange mm regn er det meldt? "))
if regn > 0.2:
    print("Da anbefaler jeg paraply!")
    print("Gjerne en paraply med NTNU-logo.")
else:
    print("Hva med ei T-skjorte med NTNU-logo?")
print("Ha en fin tur til universitetet!")
```

I dette eksemplet vil følgende skje hvis vi svarer mer enn 0.2 mm nedbør slik at betingelsen blir sann:

**Eksempel på kjøring med over 0.2 mm nedbør:**

Hvor mange mm regn er det meldt? 0.5 <br>
Da anbefaler jeg paraply! <br>
Gjerne en paraply med NTNU-logo. <br>
Ha en fin tur til universitetet! 


Svares det derimot et lavere tall, slik at betingelsen blir usann, skjer følgende:

**Eksempel på kjøring med under 0.2 mm nedbør:**  

Hvor mange mm regn er det meldt? 0.0 <br>
Hva med ei T-skjorte med NTNU-logo? <br>
Ha en fin tur til universitetet!


Altså:
- De to setningene som står på innrykk under `if` blir utført bare hvis betingelsen er sann.
- Setningen som står på innrykk under `else` blir utført bare hvis betingelsen er usann (kunne også hatt flere setninger på innrykk etter `else`).
- Den siste printen, som ikke har innrykk (Ha en fin tur...), blir utført uansett.


## **if-elif-else-setninger**
I eksemplene vi har sett hittil, har det vært kun to mulige utfall på betingelsene - kaken har stått >= 50 minutt, eller ikke; antall barn er > 0, eller ikke; alder er over 18, eller ikke. I mange praktiske situasjoner kan det være tre eller flere mulige utfall.

F.eks. hvis det var meldt mer enn 3 mm nedbør er kanskje ikke paraply tilstrekkelig, vi ville heller anbefale støvler og regntøy.  Vårt tidligere eksempel kunne da utvides som følger:

```python
regn = float(input("Hvor mange mm regn er det meldt? "))
if regn > 3.0:
    print("Da anbefaler jeg støvler og regntøy!")
elif regn > 0.2:
    print("Da anbefaler jeg paraply!")
    print("Gjerne en paraply med NTNU-logo.")
else:
    print("Hva med ei T-skjorte med NTNU-logo?")
print("Ha en fin tur til universitetet!")
```
*Under vises tre kjøringer av denne koden:*


Hvor mange mm regn er det meldt? 4.2 <br>
Da anbefaler jeg støvler og regntøy! <br>
Ha en fin tur til universitetet!

<br>



Hvor mange mm regn er det meldt? 0.5 <br>
Da anbefaler jeg paraply! <br>
Gjerne en paraply med NTNU-logo. <br>
Ha en fin tur til universitetet! <br>

<br>


Hvor mange mm regn er det meldt? 0.0 <br>
Hva med ei T-skjorte med NTNU-logo?<br>
Ha en fin tur til universitetet!

<br>

Hvis betingelsen `regn > 3.0` er sann, utføres kun printen som anbefaler støvler og regntøy, deretter går programmet til første setning etter hele if-elif-else-setningen ("Ha en fin tur...")

Hvis betingelsen `regn > 3.0` er usann, utføres **ikke** printen om støvler, programmet sjekker i stedet betingelsen på `elif`. Hvis denne er sann, skrives setningene om paraply, ellers T-skjorte som tidligere.

Det er mulig å klare seg uten if-elif-else-setninger og i stedet bare putte if-else-setninger inni hverandre for å oppnå det samme. Eksemplet ovenfor kunne alternativt skrives:

```python
# Nøstede if-else-setninger
regn = float(input("Hvor mange mm regn er det meldt? "))
if regn > 3.0:
    print("Da anbefaler jeg støvler og regntøy!")
else:
    if regn > 0.2:
        print("Da anbefaler jeg paraply!")
        print("Gjerne en paraply med NTNU-logo.")
    else:
        print("Hva med ei T-skjorte med NTNU-logo?")
print("Ha en fin tur til universitetet!")

```
Særlig i tilfeller hvor det dreier seg om tre eller flere alternative utfall basert på verdien av den **samme variabelen** (f.eks. `regn` her) vil de fleste oppfatte **if-elif-else**-setninger som både lettere å skrive og lettere å forstå, enn flere nøstede if-else-setninger.

Ett typisk eksempel er antall oppnådde poeng på en eksamen, som kan transformeres til en bokstavkarakter A-F etter gitte grenser. Det er da 6 mulige utfall, dette gir mye innrykk og blir vanskelig å lese med nøstede if-setninger:

```python
# Med nøstede if-else-setninger
score = int(input("Antall poeng: "))
  
if score >= 89:
    karakter = "A"
else:
    if score >= 77:
        karakter = "B"
    else:
        if score >= 65:
            karakter = "C"
        else:
            if score >= 53:
                karakter = "D"
            else:
                if score >= 41:
                    karakter = "E"
                else:
                    karakter = "F"
  
print("Du fikk", karakter)


#Med if-elif-else-setninger

score = int(input("Antall poeng: "))
  
if score >= 89:
    karakter = "A"
elif score >= 77:
    karakter = "B"
elif score >= 65:
    karakter = "C"
elif score >= 53:
    karakter = "D"
elif score >= 41:
    karakter = "E"
else:
    karakter = "F"
 
print("Du fikk", karakter)
```

if-elif-else-setning vil være klart å foretrekke i en slik situasjon; det er mye lettere å se at koden her tar en beslutning med 6 alternativer basert på verdien av en enkelt variabel.

Ved bruk av if-elif-else er det avgjørende at betingelsene kommer i riktig rekkefølge. Anta at vi hadde gjort karaktereksemplet motsatt:

```python
# Her har vi 
score = int(input("Antall poeng: "))
 
if score >= 0:
    karakter = "F"
elif score >= 41:
    karakter = "E"
elif score >= 53:
    karakter = "D"
elif score >= 65:
    karakter = "C"
elif score >= 77:
    karakter = "B"
elif score >= 89:
    karakter = "A"
 
print("Du fikk", karakter)
````

Her vil øverste betingelse vil være sann for alle eksamensbesvarelser - og alle ender opp med karakteren F.

Det er også viktig når det er snakk om alternativt ekskluderende utfall av samme beslutning at vi bruker **if-elif-else**-setning; **IKKE en serie med frittstående if-setninger**.

Eksempel 6 under ser nesten ut som Eksempel 4, bare at vi kun skriver `if` der vi før skrev `elif`:

```python
# HER HAR VI MED VILJE HAR GJORT FEIL
score = int(input("Antall poeng: "))
  
if score >= 89:
    karakter = "A"
if score >= 77:
    karakter = "B"
if score >= 65:
    karakter = "C"
if score >= 53:
    karakter = "D"
if score >= 41:
    karakter = "E"
else:
    karakter = "F"
 
print("Du fikk",karakter)
```
En student som har fått 92 poeng vil her komme riktig ut av den første if-setningen, og karakter settes til A. Men 92 > 77 er også sant, så karakter omgjøres deretter til B. Så til C, så til D, så til E.

De eneste tilfellene som dette programmet vil takle korrekt blir dermed studenter som skal ha E eller F.

Feilen her er at vi bruker nye frittstående if-setninger (dvs. som ikke er relatert til hverandre), mens vi egentlig har å gjøre med gjensidig ekskluderende alternativer som skulle vært løst med if-elif-else.

Hvor er det eventuelt riktig å bruke frittstående if-setninger? Jo, hvis det er snakk om flere uavhengige beslutninger. I eksempelet under er det to uavhengige beslutninger, den ene om man skal ta paraply eller ikke, den andre om brodder eller ikke. Hver beslutning tas uavhengig av den andre, da blir det riktig med to frittstående if-setninger.

Hadde vi i stedet brukt if-elif her, ville programmet ikke ha virket som det skulle, da det kun ville ha vært i stand til å anbefale brodder i oppholdsvær (mens det jo kan være minst like glatt om det regner).

```python
regn = float(input("Hvor mange mm regn er det meldt? "))
glatt = int(input("Hvor glatt er det, fra 0 (ikke) til 10 (blank is)? "))
if regn > 0.2:
    print("Da anbefaler jeg paraply.")
if glatt > 8:
    print("Da anbefaler jeg sko med brodder eller pigger.")
```
I andre tilfeller kan en beslutning være avhengig av en annen, f.eks. kun være aktuell ved et visst utfall av en foregående if-setning:

```python
regn = float(input("Hvor mange mm regn er det meldt? "))
vind = float(input("Hvor mange m/s vind er det meldt? "))
if regn > 0.2:
    if vind < 7.0:
        print("Da anbefaler jeg paraply.")
    else:
        print("Anbefaler regntøy, for mye vind for paraply.")

```

Her ville if `regn > 0.2:` .....`elif vind < 7.0` ha blitt feil.

Programmet ville da ha gjort vurderinger av vinden kun hvis det ikke regnet, og dermed ha vært ute av stand til å fraråde paraply hvis det regner og samtidig blåser kraftig.

**Oppsummert:**

- flere helt uavhengige beslutninger: bruk frittstående if-setninger
- beslutning med gjensidig utelukkende handlingsalternativer: bruk if-else (2 alternativer) eller if-elif-else (3 eller flere alternativer)
- beslutninger relatert ved at en beslutning kun er aktuell gitt et visst utfall av en annen beslutning: kan løses ved å nøste flere if-setninger inni hverandre

<BR>

<BR>


## OPPGAVER

Alle deloppgaver skal besvares her: *[Ulike typer if-setninger.py](Ulike%20typer%20if-setninger.py)* !

## a)
Du skal lage en banankake, som renger minst 50 minutter i oven. Lag et program som spør brukeren 
"Hvor mange minutter har kaken stått i ovnen?". Hvis kaken har stått i >= 50 minutter skal programme skrive ut at kaken kan tas ut av ovnen. Uansett hvor lenge kaken har stått skal den skrive ut "Tips til servering: vanlijeis."

Hvis du har løst oppgaven korekt vil tre eksempler på utskrift ved kjøring se slik ut:

---
*Hvor mange minutt har kaken stått i ovnen?* 35  
*Tips til servering: vaniljeis.*  

--- 
  
*Hvor mange minutt har kaken stått i ovnen?* 50  
*Kaken kan tas ut av ovnen.  
Tips til servering: vaniljeis.*  

---
  
*Hvor mange minutt har kaken stått i ovnen?* 65  
*Kaken kan tas ut av ovnen.  
Tips til servering: vaniljeis.*

<br>

## b) 
Ta utgangspunkt i utdelt kode ([Ulike typer if-setninger.py](Ulike%20typer%20if-setninger.py)), som ber brukeren taste inn antall epler og antall barn og deretter regner ut hvor mange epler det blir på hver. 

To eksempler på kjøring av denne koden:
___
*Hvor mange epler har du?* 14  
*Hvor mange barn passer du?* 3  
*Da blir det 4 epler til hvert barn  
og 2 epler til overs til deg selv.  
Takk for i dag!*

___

*Hvor mange epler har du?* 4  
*Hvor mange barn passer du?* 0  
Traceback (most recent call last):  
  ...  
ZeroDivisionError: integer division or modulo by zero  

___

Det første, hvor brukeren skriver inn tallene 14 og 3, funker fint. Hvis brukeren derimot svarer 0 barn, vil programmet gi feilmelding (`ZeroDivisionError`) fordi det er umulig å dele på null. Negativt antall barn vil "funke", men ikke gi mening.

Din oppgave er derfor å endre koden slik at de to print-setningene som forteller hvor mange epler det blir til barna og deg selv kun utføres hvis barn > 0. "Takk for i dag" skal printes uansett. 

<br>

## c)
I denne deloppgaven skal det lages et program som sjekker om en person kan stemme, altså om personen er 18 år eller eldre. Man må da spørre brukeren om alder og lagre svaret i en variabel, for deretter å sjekke om brukerens alder er 18 eller mer. Programmet skal så gi tilbakemelding til brukeren om den kan stemme eller ikke. 

Eksempel på kjøring ved korrekt implementasjon:

*Skriv inn din alder:* 19 <br>
*Du kan stemme :)*

---
*Skriv inn din alder:* 15 <br>
*Du kan ikke stemme ennå*

<br>

<br>


## d)
Lag en utvidet versjon av programmet fra (c) som sier om personen kan stemme eller ikke, med følgende regler:

- `alder >= 18`: Kan stemme både ved lokalvalg og Stortingsvalg
- `alder >= 16`: Kan stemme ved lokalvalg, men ikke ved Stortingsvalg
- ellers (`alder < 16`): Kan ikke stemme.

Eksempel på kjøring:


*Skriv inn din alder:* 19 <br>
*Du kan stemme både ved lokalvalg og Stortingsvalg*

___
*Skriv inn din alder:* 17 <br>
*Du kan stemme ved lokalvalg, men ikke ved Stortingsvalg*
___
*Skriv inn din alder:* 12 <br>
*Du kan ikke stemme ennå*


<br>
 <br>

## e)
Du skal lage et program som sjekker hvilken billettpris en person skal betale ut ifra alderen til personen.

Oversikt over alder og tilhørende billettpriser:

Aldersgruppe | Billettpris
--- | ---
Under 3 år | Gratis
3-11 år | 30 kr
12-25 år | 50 kr
26-66 år | 80 kr
67 år og over | 40 kr

<br>

***Tips*** - Dette er en beslutning med gjensidig utelukkende handlingsalternativer. Den har flere enn 2 alternativer, og det vil derfor være naturlig å bruke if-elif-else. 
