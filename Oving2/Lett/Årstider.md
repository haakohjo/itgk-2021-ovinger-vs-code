### **[Intro øving 2](../Intro_Øving2.md)**
<br>

# Årstider

**Læringsmål:**
- Betingelser
- Logiske uttrykk

**Starting Out with Python:**
- Kap. 3.3-3.5


## OPPGAVE
Oppgaven skal besvares her: *[Årstider.py](Årstider.py)* !

I denne oppgaven skal en bruker skrive inn dag og måned og få ut hvilken årstid datoen tilhører.

Et år har (offisielt) fire årstider, og i denne oppgaven tar vi utgangspunkt i at årstidsskiftene følger tabellen nedenfor. **(Merk deg datoene)**


Årstid | Første dag
--- | ---
Vår | 20. mars
Sommer | 21. juni
Høst | 22. september
Vinter | 21. desember

<br>

Lag et program som tar inn en måned som en streng og en dag i denne måneden som et tall fra brukeren. Programmet skal så skrive ut årstiden assosiert med denne datoen.

Du kan anta at inputen er en gyldig dato.

**Eksempel på kjøring:**
___
*Måned:* mars <br>
*Dag:* 20 <br>
*Vår*
___
*Måned:* mars <br>
*Dag:* 19 <br>
*Vinter*
___
*Måned:* november <br>
*Dag:* 20 <br>
*Høst*
