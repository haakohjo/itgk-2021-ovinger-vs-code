### **[Intro øving 2](../Intro_Øving2.md)**
<br>

# Forbrytelse og straff

**Læringsmål:**
- Kodeforståelse, hvordan tenke om oppsett av if-setninger
- Kunne forstå og rette typiske feil med if-setninger

**Starting Out with Python:**
- Kap. 3.1-3.2

I denne oppgaven skal vi rette kodefeil og lære hvordan man tenker om strukturen av både enkle og komplekse if-setninger.

## INTRODUKSJON

### **Noen typiske syntaksfeil** <br>
Noen typiske syntaksfeil som nybegynnere gjerne gjør i bruk av if-setninger:

- Glemme kolon bakerst i linjer med `if`, `else` og `elif`. Det må være kolon `:` disse stedene
- Feil med logiske operatorer, f.eks. bruke tilordning = når man egentlig mener sammenligning `==`. Eller unødig mellomrom i operatorer som skrives med to symboler, som `> =`, disse må stå helt inntil hverandre
- Feil med parenteser som ikke matcher i sammensatte logiske uttrykk med `and`, `or`, `not`
- Feil med marg / innrykk - alle setninger som hører til en if må stå med eksakt ett tabulator-innrykk, en else må stå på eksakt samme marg som den if den hører sammen med, etc. Bare ett mellomrom i forskjell vil være nok til at koden feiler.

<br>

### **Enkle logiske feil** <br>
Vi går nå videre med enkle logiske feil, som vil føre til at if-setninger kommer til feil konklusjon. Typiske eksempler på slike feil:

**(I)** Bruk av feil sammenligningsoperator, f.eks. < i stedet for >, eller > i stedet for >=. Koden under viser ett eksempel på dette:

Eks:
```python
alder = int(input("Alder: "))
if alder > 18:
    print("Kunden har lov til å kjøpe vin")
```
Her vil kunden måtte være 19 eller mer for en positiv konklusjon, mens det skulle holdt med 18. Dvs., operatoren skulle vært `>=` , ikke `>`

<br>


**(II)** Bruk av feil operator (typisk forveksle `and` og `or`) i sammensatte betingelser, eller feil / manglende parentesbruk i sammensatte betingelser. Koden under viser to feilaktige forsøk på å beslutte om en kunde kan kjøpe vin eller ikke (forrige feil med `>=` rettet):

Eks:
```python
# FORSØK 1:
alder = int(input("Alder: "))
ruset = input("Er kunden beruset? (J/N) ")
um = input("Er kunden umyndiggjort? (J/N) ")
if alder >= 18 or ruset == "N" or um == "N":
    print("Kunden har lov til å kjøpe vin")
  
# FORSØK 2:
alder = int(input("Alder: "))
ruset = input("Er kunden beruset? (J/N) ")
um = input("Er kunden umyndiggjort? (J/N) ")
if alder >= 18 and not ruset == "J" or um == "J":
    print("Kunden har lov til å kjøpe vin")
```
**Forsøk 1** feiler her fordi man har brukt `or` der man skulle brukt `and`. Alle de tre betingelsene må være tilfredsstilt, siden Polet ikke selger vin til noen som er synlig ruset, eller som man vet er umyndiggjort.

**Forsøk 2** feiler fordi `or` har lavere presedens enn `not` og `and`. Python vil derfor først sjekke betingelsen `alder >= 18 and not ruset=="J"`, deretter vil den sette hele dette sammen med `or um == "J"`. Dette vil føre til at en person som er umyndiggjort vil få lov til å kjøpe vin uansett, også om man er under 18 og ruset. For at denne betingelsen skal evalueres korrekt må man sette parentes rundt de to betingelsene som har `or` mellom seg som vist nedenfor. Da vil disse to bli sammenholdt først, og begge disse vil bli berørt av det foregående `not`, altså at man **ikke** må være umyndiggjort.

En korrekt løsning ville vært:

```python
# KORREKT LØSNING, FORSØK 1:
alder = int(input("Alder: "))
ruset = input("Er kunden beruset? (J/N) ")
um = input("Er kunden umyndiggjort? (J/N) ")
if alder >= 18 and ruset == "N" and um == "N":
    print("Kunden har lov til å kjøpe vin")


# KORREKT LØSNING, FORSØK 2:
alder = int(input("Alder: "))
ruset = input("Er kunden beruset? (J/N) ")
um = input("Er kunden umyndiggjort? (J/N) ")
if alder >= 18 and not (ruset == "J" or um == "J"):
    print("Kunden har lov til å kjøpe vin")
```

FORSØK 1 og 2 gir eksakt samme resultat - det vil ofte være flere mulige måter å skrive sammensatte betingelser på. I dette tilfellet kan man vel si at Variant 1 er å foretrekke, siden den slipper å bruke `not` og parentes, og dermed blir litt lettere å lese.

<br>

### **Rekkefølge if-elif-else-setninger** <br>
Ved bruk av if-elif-else er det avgjørende at betingelsene kommer i riktig rekkefølge. I Eksempel 1 under skal vi regne ut karakter fra poengscore på en eksamen (hvor toppscore er 100 og blank besvarelse gir 0 poeng). På grunn av feil rekkefølge på testene vil programmet ikke funke som det skal. Problemet er at den første testen, `score >= 0`, vil slå til for absolutt alle eksamenskandidater - og de andre betingelse etter `elif` vil dermed aldri bli evaluert. Alle kandidatene ender da med F.

```python
# DENNE KODEN INNHOLDER FEIL
score = int(input("Antall poeng: "))
  
if score >= 0:
    karakter = "F"
elif score >= 41:
    karakter = "E"
elif score >= 53:
    karakter = "D"
elif score >= 65:
    karakter = "C"
elif score >= 77:
    karakter = "B"
elif score >= 89:
    karakter = "A"
  
print("Du fikk", karakter)
```
Setter vi derimot testene motsatt rekkefølge, vil det funke. Generelt er det viktig å sørge for at alle alternativene i en if-elif-else kan nås gitt en passende verdi. Her vil studenter med `score >= 89` få A mens de med lavere score vil bli fordelt videre nedover i if-elif-strukturen.

```python
# MED KORRIGERT REKKEFØLGE
score = int(input("Antall poeng: "))
   
if score >= 89:
    karakter = "A"
elif score >= 77:
    karakter = "B"
elif score >= 65:
    karakter = "C"
elif score >= 53:
    karakter = "D"
elif score >= 41:
    karakter = "E"
else:
    karakter = "F"
  
print("Du fikk", karakter)
```
<br>

### **Feil valg av struktur for if-setninger**
Ofte skal vi ikke treffe bare én beslutning men flere forskjellige beslutninger i samme program. Dette tilsier bruk av flere if-setninger, og det er viktig å finne fram til en korrekt struktur for if-setningene. Her skal vi se på forskjellen mellom tre ulike situasjoner:

1. beslutning med alternative handlinger som er gjensidig utelukkende: Bruk **if-else** (hvis 2 alternativer) eller **if-elif-else** (hvis 3 eller flere alternativer). 
2. flere beslutninger som er uavhengige av hverandre: **Bruk frittstående if-setninger**
3. beslutninger som er avhengig av andre beslutninger, dvs. bare relevant å vurdere gitt et visst utfall av en tidligere beslutning: Kan løses ved **nøstede if-setninger**.

Hvis man velger feil struktur på if-setningene, vil man få programmer som tar feil beslutninger.

Du kan lese mer om dette under introduksjonen til [ulike typer if-setninger](Ulike%20typer%20if-setninger.md).


<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Forbrytelse og straff.py](Forbrytelse%20og%20straff.py)* !

## a) Syntaksfeil
I denne oppgaven har dere fått utdelt en kode. Som du vil se, inneholder den syntaksfeil i de nederste linjene (if-else-setningen).

***Rett alle syntaksfeilene, slik at koden kjører*** og gir en konklusjon ut på skjermen (om det var promillekjøring eller ikke) – for denne deloppgaven behøver ikke konklusjonen være riktig, det er nok å få programmet til å kjøre. NB! Du skal **ikke** slette noen kodelinjer eller betingelser!

<br>

## b) Enkle logiske feil
***Jobb videre med koden fra oppgave (a) hvor du korrigerte syntaksfeilene. Prøv ut programmet med følgende input:***

- promille = 0.1, motor = "j", fører = "j", ledsager = "n", nødrett = "n". Du vil da se at programmet feilaktig sier at det var promillekjøring selv om promillen var for liten. ***Rett denne feilen (men ikke noe annet)***, kjør programmet med samme input og se at det nå konkluderer rett, test også med promille 0.3 men ellers samme input og se at det gjør rett med dette også. 
- Kjør deretter programmet med følgende input: promille = 0.1, motor = "j", fører = "n", ledsager = "j", nødrett = "n". Du vil se at programmet igjen feilaktig konkluderer med promillekjøring, til tross for rettingen du gjorde i sted. Dette skyldes en litt mer komplisert feil i betingelsen. ***Rett denne feilen*** og test deretter at koden nå virker som den skal med litt ulike kombinasjoner av verdier.


<br>

## c) Feil i rekkefølge på if-elif-setninger
For denne oppgaven har dere fått utdelt enda en kodesnutt. Det er en ny variant av promilleprogrammet. For kjappere testing ser vi nå bort fra spørsmålene om det var motorvogn, om vedkommende førte kjøretøyet, og eventuell nødrett - dvs. vi antar at alt dette i boks fra før, og spørsmål dermed bare er størrelse på promillen og resulterende straff. For at du skal slippe å skrive inn verdier for varabler som er irrelevante for denne oppgaven, kan disse kommenteres ut i .py filen. 

Kjør programmet nedenfor et par ganger med ulike verdier, f.eks. med promille 0.1 deretter 0.3 deretter 0.9.

Som du vil se, vil man aldri få større forelegg enn 6000,- med dette programmet, uansett hvor høy promillen var. Hvis promillen var over 0.4, f.eks. 0.45, skulle man hatt 10000,- i forelegg, og med promille høyere enn 0.5 skulle man hatt bot basert på månedslønn, samt fengselsstraff.

***Korriger programmet*** så det gjør riktige beslutninger på straffenivå for alle mulige verdier av promille.

<br>

## d) Feil i valg av overordnet struktur på if-setninger
I den neste kodesnutten ser vi på et relatert problem, nemlig hvor lenge førerkortet skal inndras. Alt i alt er reglene som følger:
- Førerkort skal inndras på livstid dersom det er gjentatt promillekjøring (dømt for det også tidligere), eller hvis man slo seg vrang og nektet og samarbeide ved legetesten, og dette skjer mindre enn 5 år etter en tidligere dom for promillekjøring. (Dvs., hvis man slår seg vrang ved legetesten, kan man få inndratt førerkortet selv om blodprøven deretter viser at promillen var under 0.2.)
- For personer som **ikke** er tidligere dømt for promillekjøring, gjelder følgende:
  - Hvis promillen var over 1.2, inndras førerkort for minst 2 år. Inndragelse for 2 år gis også hvis man slår seg vrang under legetesten - da uavhengig av hva promillen var.
  - Promille i området 0.8-1.2 gir inndragning i 20-22 måneder.
  - Promille 0.5-0.8 gir vanligvis 18 måneder.
  - Promille 0.2-0.5 gir inntil 1 års inndragelse.

Koden under skulle implementere disse reglene, men gjør feil i noen tilfeller. 

- Hvis det svares "j" på "Tidligere dømt..." sier programmet at førerkort skal inndras på livstid uansett hva det svares på de andre spørsmålene, dvs. også om promille var 0 og man oppførte seg pent ved legetesten.
- Med f.eks. 1.4 eller 0.9 for promille (og "n" på tidligere dømt, "n" på nektet å samarbeide) vil programmet først skrive ut korrekt straffereaksjon, men vil deretter fortsette med å skrive ut også mildere straffereaksjoner. 

***Rett disse feilene så programmet gir riktig konklusjon på straffereaksjonen for alle mulige kombinasjoner av input-verdier.***